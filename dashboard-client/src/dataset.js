export default {
    "employers": [
        {
            "er_no": 9001,
            "name": "AW Demo"
        },
        {
            "er_no": 9002,
            "name": "Sprocket Co"
        },
        {
            "er_no": 9003,
            "name": "Widgets Inc"
        },
        {
            "er_no": 9004,
            "name": "ACME"
        }
    ],

    "plan_periods": [
        {
            "plan_period_no": 150,
            "er_no": 9001,
            "start_date": "2008-01-01",
            "end_date": "2009-12-31",
            "description": "2008 Plan Year"
        },
        {
            "plan_period_no": 850,
            "er_no": 9001,
            "start_date": "2010-01-01",
            "end_date": "2010-12-31",
            "description": "2010 Plan Year"
        },
        {
            "plan_period_no": 1310,
            "er_no": 9001,
            "start_date": "2011-01-01",
            "end_date": "2011-12-31",
            "description": "2011 Plan Year"
        },
        {
            "plan_period_no": 170,
            "er_no": 9001,
            "start_date": "2009-01-01",
            "end_date": "2009-12-31",
            "description": "2009 Plan Year"
        },
        {
            "plan_period_no": 2360,
            "er_no": 9001,
            "start_date": "2012-01-01",
            "end_date": "2012-12-31",
            "description": "2012 Plan Year"
        },
        {
            "plan_period_no": 3240,
            "er_no": 9001,
            "start_date": "2013-01-01",
            "end_date": "2013-12-31",
            "description": "2013 Plan Year"
        },
        {
            "plan_period_no": 5330,
            "er_no": 9001,
            "start_date": "2014-01-01",
            "end_date": "2014-12-31",
            "description": "2014 Plan Year"
        },
        {
            "plan_period_no": 6590,
            "er_no": 9001,
            "start_date": "2015-01-01",
            "end_date": "2015-12-31",
            "description": "2015 Plan Year"
        },
        {
            "plan_period_no": 7480,
            "er_no": 9001,
            "start_date": "2016-01-01",
            "end_date": "2016-12-31",
            "description": "2016 Plan Year"
        },
        {
            "plan_period_no": 9640,
            "er_no": 9001,
            "start_date": "2017-01-01",
            "end_date": "2017-12-31",
            "description": "2017 Plan Year"
        },
        {
            "plan_period_no": 11620,
            "er_no": 9001,
            "start_date": "2018-01-01",
            "end_date": "2018-12-31",
            "description": "2018 Plan Year"
        }
    ],

    "employees": [
        {
            "ee_id": "EX0626295200",
            "er_no": 9001,
            "first_name": "Charles",
            "last_name": "Darwin",
            "middle_name": "Robert",
            "email": "somebody@testmail.com",
            "birth_date": "1960-02-14",
            "gender": "M",
            "is_test": false
        },
        {
            "ee_id": "EX0626295300",
            "er_no": 9001,
            "first_name": "Amelia",
            "last_name": "Earhardt",
            "middle_name": "Mary",
            "email": "aearhard@testmail.com",
            "birth_date": "1953-07-24",
            "gender": "F",
            "is_test": false
        },
        {
            "ee_id": "EX0626295400",
            "er_no": 9001,
            "first_name": "Cobra",
            "last_name": "Basic",
            "middle_name": "Leigh",
            "email": "zerman@cordovaware.com",
            "birth_date": "1972-04-20",
            "gender": "F",
            "is_test": false
        },
        {
            "ee_id": "EX0628370100",
            "er_no": 9001,
            "first_name": "Lindsay",
            "last_name": "Vonn",
            "middle_name": "",
            "email": "herman@cordovaware.com",
            "birth_date": "1984-10-18",
            "gender": "M",
            "is_test": false
        },
        {
            "ee_id": "EX0628372300",
            "er_no": 9001,
            "first_name": "Test",
            "last_name": "Employee",
            "middle_name": "",
            "email": "zerman@yahoo.com",
            "birth_date": "1984-07-11",
            "gender": "F",
            "is_test": false
        },
        {
            "ee_id": "EX0628372400",
            "er_no": 9001,
            "first_name": "Ben",
            "last_name": "Agosto",
            "middle_name": "Robert",
            "email": "ok@ok.com",
            "birth_date": "1982-01-15",
            "gender": "M",
            "is_test": false
        },
        {
            "ee_id": "EX0635705600",
            "er_no": 9001,
            "first_name": "Tom",
            "last_name": "Brady",
            "middle_name": "",
            "email": "a@b.com",
            "birth_date": "1979-05-12",
            "gender": "M",
            "is_test": false
        },
        {
            "ee_id": "EX0636571700",
            "er_no": 9001,
            "first_name": "Sherlock",
            "last_name": "Holmes",
            "middle_name": "",
            "email": "test@test.com",
            "birth_date": "1970-01-06",
            "gender": "M",
            "is_test": false
        },
        {
            "ee_id": "EX0640693900",
            "er_no": 9001,
            "first_name": "John",
            "last_name": "Doe",
            "middle_name": "",
            "email": "test@test.com",
            "birth_date": "1983-09-26",
            "gender": "F",
            "is_test": false
        },
        {
            "ee_id": "EX0640866400",
            "er_no": 9001,
            "first_name": "John D",
            "last_name": "Munu",
            "middle_name": "",
            "email": "john@Munu.com",
            "birth_date": "1978-07-17",
            "gender": "M",
            "is_test": false
        },
        {
            "ee_id": "EX0641398200",
            "er_no": 9001,
            "first_name": "Dana",
            "last_name": "Testor",
            "middle_name": "Marie",
            "email": "dwilson@cordovaware.com",
            "birth_date": "1978-07-17",
            "gender": "F",
            "is_test": false
        },
        {
            "ee_id": "EX0650375100",
            "er_no": 9001,
            "first_name": "Me",
            "last_name": "Delete",
            "middle_name": "",
            "email": "zerman@cordovaware.com",
            "birth_date": "1967-08-03",
            "gender": "M",
            "is_test": false
        },
        {
            "ee_id": "EX0655346300",
            "er_no": 9001,
            "first_name": "Roman",
            "last_name": "Munu",
            "middle_name": "",
            "email": "herman@cordovaware.com",
            "birth_date": "1990-07-06",
            "gender": "M",
            "is_test": false
        },
        {
            "ee_id": "EX0655346400",
            "er_no": 9001,
            "first_name": "Emilio",
            "last_name": "Munu",
            "middle_name": "",
            "email": "herman@cordovaware.com",
            "birth_date": "1966-12-23",
            "gender": "M",
            "is_test": false
        },
        {
            "ee_id": "EX0655346500",
            "er_no": 9001,
            "first_name": "Xiomara",
            "last_name": "Munu",
            "middle_name": "",
            "email": "herman@cordovaware.com",
            "birth_date": "1973-08-11",
            "gender": "F",
            "is_test": false
        },
        {
            "ee_id": "EX0655346600",
            "er_no": 9001,
            "first_name": "Alba",
            "last_name": "Munu",
            "middle_name": "",
            "email": "herman@cordovaware.com",
            "birth_date": "1950-02-27",
            "gender": "F",
            "is_test": false
        },
        {
            "ee_id": "EX0655346700",
            "er_no": 9001,
            "first_name": "Sin",
            "last_name": "Munu",
            "middle_name": "",
            "email": "herman@cordovaware.com",
            "birth_date": "1978-10-10",
            "gender": "F",
            "is_test": false
        },
        {
            "ee_id": "EX0655346800",
            "er_no": 9001,
            "first_name": "Petra",
            "last_name": "Munu",
            "middle_name": "",
            "email": "herman@cordovaware.com",
            "birth_date": "1986-09-23",
            "gender": "F",
            "is_test": false
        },
        {
            "ee_id": "EX0655346900",
            "er_no": 9001,
            "first_name": "Rogelio",
            "last_name": "Munu",
            "middle_name": "",
            "email": "herman@cordovaware.com",
            "birth_date": "1972-03-19",
            "gender": "M",
            "is_test": false
        },
        {
            "ee_id": "EX0655347000",
            "er_no": 9001,
            "first_name": "Michael",
            "last_name": "Munu",
            "middle_name": "",
            "email": "herman@cordovaware.com",
            "birth_date": "1989-02-07",
            "gender": "M",
            "is_test": false
        },
        {
            "ee_id": "EX0655347100",
            "er_no": 9001,
            "first_name": "Rafael",
            "last_name": "Munu",
            "middle_name": "",
            "email": "herman@cordovaware.com",
            "birth_date": "1990-05-04",
            "gender": "M",
            "is_test": false
        },
        {
            "ee_id": "EX0655347200",
            "er_no": 9001,
            "first_name": "Jane",
            "last_name": "Munu",
            "middle_name": "",
            "email": "herman@cordovaware.com",
            "birth_date": "1992-06-15",
            "gender": "F",
            "is_test": false
        },
        {
            "ee_id": "EX0668484500",
            "er_no": 9001,
            "first_name": "Danielle",
            "last_name": "Teztor",
            "middle_name": "",
            "email": "dmodzelewski@cordovaware.com",
            "birth_date": "1983-10-25",
            "gender": "F",
            "is_test": false
        },
        {
            "ee_id": "TS0626295200",
            "er_no": 9001,
            "first_name": "Charles",
            "last_name": "Darwin",
            "middle_name": "Robert",
            "email": "charles.darwin@cordovaware.com",
            "birth_date": "1960-02-14",
            "gender": "M",
            "is_test": true
        },
        {
            "ee_id": "TS0641398200",
            "er_no": 9001,
            "first_name": "Dana",
            "last_name": "Testor",
            "middle_name": "Marie",
            "email": "dwilson@cordovaware.com",
            "birth_date": "1978-07-17",
            "gender": "F",
            "is_test": true
        },

        {
            "ee_id": "EX9000000007",
            "er_no": 9002,
            "first_name": "Monkey",
            "last_name": "Luffy",
            "middle_name": "D",
            "email": "mdluffy@goingmerry.com",
            "birth_date": "1978-07-17",
            "gender": "M",
            "is_test": false
        },
        {
            "ee_id": "EX9000000008",
            "er_no": 9002,
            "first_name": "Roronoa",
            "last_name": "Zoro",
            "middle_name": "L",
            "email": "rzoro@goingmerry.com",
            "birth_date": "1978-07-17",
            "gender": "M",
            "is_test": false
        },
        {
            "ee_id": "EX9000000009",
            "er_no": 9002,
            "first_name": "Nami",
            "last_name": "Schwan",
            "middle_name": "",
            "email": "nschwan@goingmerry.com",
            "birth_date": "1978-07-17",
            "gender": "F",
            "is_test": false
        }
    ],
/*
    "employee_details": [
        {
            "ee_id": "TS0626295200",
            "type": "census",
            "plan_period_no": 150,
            "data": "{\"employment_status\":\"A\",\"hire_date\":\"1989-07-10\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"55900.00\",\"annual_income\":\"55900.00\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"IT\"}"
        },
        {
            "ee_id": "EX0626295200",
            "type": "census",
            "plan_period_no": 150,
            "data": "{\"employment_status\":\"A\",\"hire_date\":\"1989-07-10\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"55900.00\",\"annual_income\":\"55900.00\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"IT\"}"
        },
        {
            "ee_id": "EX0626295300",
            "type": "census",
            "plan_period_no": 150,
            "data": "{\"employment_status\":\"A\",\"hire_date\":\"2005-05-13\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"24735.50\",\"annual_income\":\"24735.50\",\"pay_periods\":\"52\",\"hours_worked\":\"32\",\"hourly\":\"Y\",\"location\":\"WI\",\"department\":\"S\",\"enrollment_mode\":\"OE\",\"dental_lock_override\":\"N\",\"is_it_sunny\":\"U\",\"favorite_team\":\"U\",\"other_favorite_team\":\"U\",\"favorite_coffee\":\"S\",\"weather_today\":\"U\"}"
        },
        {
            "ee_id": "TS0628372400",
            "type": "census",
            "plan_period_no": 850,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"hire_date\":\"2008-06-01\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"36999\",\"annual_income\":\"36999\",\"pay_periods\":\"52\",\"hours_worked\":\"40.0\",\"pay_schedule\":\"pay_schedule_2\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"HR\",\"hs2_code\":\"N\",\"municipality\":\"AK\",\"work_state\":\"U\",\"nationality\":\"US\",\"dental_lock_override\":\"N\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\"}"
        },
        {
            "ee_id": "TS0626295200",
            "type": "census",
            "plan_period_no": 850,
            "data": "{\"employment_status\":\"A\",\"hire_date\":\"1989-07-10\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"55900.00\",\"annual_income\":\"55900.00\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"IT\",\"enrollment_mode\":\"OE\",\"dental_lock_override\":\"N\",\"eye_color\":\"BL\",\"hair_color\":\"N\",\"work_state\":\"U\",\"nationality\":\"US\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"pay_schedule\":\"pay_schedule_1\",\"hs2_code\":\"N\"}"
        },
        {
            "ee_id": "EX0626295300",
            "type": "census",
            "plan_period_no": 850,
            "data": "{\"employment_status\":\"A\",\"hire_date\":\"2005-05-13\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"24735.50\",\"annual_income\":\"24735.50\",\"pay_periods\":\"52\",\"hours_worked\":\"32\",\"hourly\":\"Y\",\"location\":\"WI\",\"department\":\"S\",\"enrollment_mode\":\"OE\",\"pay_schedule\":\"pay_schedule_2\",\"hs2_code\":\"N\",\"work_state\":\"U\",\"nationality\":\"US\",\"dental_lock_override\":\"N\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\"}"
        },
        {
            "ee_id": "EX0628370100",
            "type": "census",
            "plan_period_no": 850,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"hire_date\":\"2002-02-01\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"76525\",\"annual_income\":\"76525\",\"pay_periods\":\"52\",\"hours_worked\":\"40.0\",\"hourly\":\"N\",\"location\":\"WI\",\"department\":\"S\",\"hs2_code\":\"N\",\"municipality\":\"AK\",\"work_state\":\"U\",\"nationality\":\"US\",\"dental_lock_override\":\"N\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"pay_schedule\":\"pay_schedule_2\"}"
        },
        {
            "ee_id": "EX0628372400",
            "type": "census",
            "plan_period_no": 850,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"hire_date\":\"2008-06-01\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"36999\",\"annual_income\":\"36999\",\"pay_periods\":\"52\",\"hours_worked\":\"40.0\",\"pay_schedule\":\"pay_schedule_2\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"HR\",\"hs2_code\":\"N\",\"municipality\":\"AK\",\"work_state\":\"U\",\"nationality\":\"US\",\"dental_lock_override\":\"N\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\"}"
        },
        {
            "ee_id": "EX0628372300",
            "type": "census",
            "plan_period_no": 850,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"hire_date\":\"2006-05-01\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"36799\",\"annual_income\":\"36799\",\"pay_periods\":\"52\",\"hours_worked\":\"40.0\",\"pay_schedule\":\"pay_schedule_2\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"HR\",\"hs2_code\":\"N\",\"municipality\":\"AK\",\"work_state\":\"U\",\"nationality\":\"US\",\"dental_lock_override\":\"N\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\"}"
        },
        {
            "ee_id": "EX0626295200",
            "type": "census",
            "plan_period_no": 850,
            "data": "{\"employment_status\":\"A\",\"hire_date\":\"1989-07-10\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"55900.00\",\"annual_income\":\"55900.00\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"IT\",\"enrollment_mode\":\"OE\",\"dental_lock_override\":\"N\",\"eye_color\":\"BL\",\"hair_color\":\"N\",\"work_state\":\"U\",\"nationality\":\"US\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"pay_schedule\":\"pay_schedule_1\",\"hs2_code\":\"N\"}"
        },
        {
            "ee_id": "EX0626295400",
            "type": "census",
            "plan_period_no": 850,
            "data": "{\"enrollment_mode\":\"LE\",\"employment_status\":\"A\",\"hire_date\":\"2009-02-16\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"74999.99\",\"annual_income\":\"74999.99\",\"pay_periods\":\"26\",\"hours_worked\":\"24\",\"hourly\":\"N\",\"location\":\"FL\",\"department\":\"S\",\"dental_lock_override\":\"N\",\"pay_schedule\":\"pay_schedule_1\",\"hs2_code\":\"N\",\"work_state\":\"U\",\"nationality\":\"US\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\"}"
        },
        {
            "ee_id": "TS0628372400",
            "type": "census",
            "plan_period_no": 1310,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"hire_date\":\"2008-06-01\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"36999.00\",\"annual_income\":\"36999.00\",\"pay_periods\":\"52\",\"hours_worked\":\"40.0\",\"pay_schedule\":\"pay_schedule_2\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"HR\",\"hs2_code\":\"N\",\"municipality\":\"AK\",\"work_state\":\"U\",\"nationality\":\"US\",\"dental_lock_override\":\"N\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"smoker\":\"N\",\"pay_rate\":\"\",\"weather\":\"N\",\"favorite_state\":\"U\",\"custom_2\":\"N\"}"
        },
        {
            "ee_id": "TS0626295200",
            "type": "census",
            "plan_period_no": 1310,
            "data": "{\"employment_status\":\"A\",\"hire_date\":\"1989-07-10\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"55900.00\",\"annual_income\":\"55900.00\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"IT\",\"enrollment_mode\":\"OE\",\"dental_lock_override\":\"N\",\"eye_color\":\"BL\",\"hair_color\":\"N\",\"work_state\":\"U\",\"nationality\":\"US\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"pay_schedule\":\"pay_schedule_1\",\"hs2_code\":\"N\",\"credit_mode\":\"N\",\"salaried\":\"Y\",\"smoker\":\"N\",\"pay_rate\":\"\",\"weather\":\"WIC\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\"}"
        },
        {
            "ee_id": "EX0626295300",
            "type": "census",
            "plan_period_no": 1310,
            "data": "{\"employment_status\":\"A\",\"hire_date\":\"2005-05-13\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"24735.50\",\"annual_income\":\"24735.50\",\"pay_periods\":\"52\",\"hours_worked\":\"32\",\"hourly\":\"Y\",\"location\":\"WI\",\"department\":\"S\",\"enrollment_mode\":\"OE\",\"pay_schedule\":\"pay_schedule_2\",\"hs2_code\":\"N\",\"work_state\":\"U\",\"nationality\":\"US\",\"dental_lock_override\":\"N\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\"}"
        },
        {
            "ee_id": "EX0626295400",
            "type": "census",
            "plan_period_no": 1310,
            "data": "{\"enrollment_mode\":\"LE\",\"employment_status\":\"A\",\"hire_date\":\"2009-02-16\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"74999.99\",\"annual_income\":\"74999.99\",\"pay_periods\":\"26\",\"hours_worked\":\"24\",\"hourly\":\"N\",\"location\":\"FL\",\"department\":\"S\",\"dental_lock_override\":\"N\",\"pay_schedule\":\"pay_schedule_1\",\"hs2_code\":\"N\",\"work_state\":\"U\",\"nationality\":\"US\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\"}"
        },
        {
            "ee_id": "EX0628370100",
            "type": "census",
            "plan_period_no": 1310,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"hire_date\":\"2002-02-01\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"76525\",\"annual_income\":\"76525\",\"pay_periods\":\"52\",\"hours_worked\":\"40.0\",\"hourly\":\"N\",\"location\":\"WI\",\"department\":\"S\",\"hs2_code\":\"N\",\"municipality\":\"AK\",\"work_state\":\"U\",\"nationality\":\"US\",\"dental_lock_override\":\"N\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"pay_schedule\":\"pay_schedule_2\"}"
        },
        {
            "ee_id": "EX0628372300",
            "type": "census",
            "plan_period_no": 1310,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"hire_date\":\"2006-05-01\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"36799\",\"annual_income\":\"36799\",\"pay_periods\":\"52\",\"hours_worked\":\"40.0\",\"pay_schedule\":\"pay_schedule_2\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"HR\",\"hs2_code\":\"N\",\"municipality\":\"AK\",\"work_state\":\"U\",\"nationality\":\"US\",\"dental_lock_override\":\"N\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\"}"
        },
        {
            "ee_id": "EX0628372400",
            "type": "census",
            "plan_period_no": 1310,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"hire_date\":\"2008-06-01\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"36999.00\",\"annual_income\":\"36999.00\",\"pay_periods\":\"52\",\"hours_worked\":\"40.0\",\"pay_schedule\":\"pay_schedule_2\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"HR\",\"hs2_code\":\"N\",\"municipality\":\"AK\",\"work_state\":\"U\",\"nationality\":\"US\",\"dental_lock_override\":\"N\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"smoker\":\"N\",\"pay_rate\":\"\",\"weather\":\"N\",\"favorite_state\":\"U\",\"custom_2\":\"N\"}"
        },
        {
            "ee_id": "EX0626295200",
            "type": "census",
            "plan_period_no": 1310,
            "data": "{\"employment_status\":\"A\",\"hire_date\":\"1989-07-10\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"55900.00\",\"annual_income\":\"55900.00\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"IT\",\"enrollment_mode\":\"OE\",\"dental_lock_override\":\"N\",\"eye_color\":\"BL\",\"hair_color\":\"N\",\"work_state\":\"U\",\"nationality\":\"US\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"pay_schedule\":\"pay_schedule_1\",\"hs2_code\":\"N\",\"credit_mode\":\"N\",\"salaried\":\"Y\",\"smoker\":\"N\",\"pay_rate\":\"\",\"weather\":\"WIC\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\"}"
        },
        {
            "ee_id": "TS0626295200",
            "type": "census",
            "plan_period_no": 170,
            "data": "{\"employment_status\":\"A\",\"hire_date\":\"2009-06-01\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"55900.00\",\"annual_income\":\"55900.00\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"IT\",\"enrollment_mode\":\"OE\",\"dental_lock_override\":\"N\",\"work_state\":\"U\",\"nationality\":\"US\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"enrollment_status\":\"not_enrolled\",\"enrolled_by\":\"N\\\/A\",\"enrollment_date\":\"N\\\/A\",\"orig_enrollment_date\":\"N\\\/A\",\"has_beneficiaries\":\"Y\",\"has_dependents\":\"Y\",\"is_test_employee\":\"N\",\"overall_eligibility_date\":\"2008-01-01\",\"term_date\":\"\",\"rehire_date\":\"\",\"pay_frequency\":\"\",\"pay_rate\":\"\",\"salaried\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"\",\"division\":\"\",\"service_years\":\"-1\",\"actual_age\":\"49\",\"benefit_age\":\"48\",\"imputed_age\":\"49\",\"pay_schedule\":\"\",\"previous_pay_date\":\"\",\"next_pay_date\":\"\",\"remaining_pay_periods\":\"\",\"now_date\":\"2010-01-06\",\"now_time\":\"08:57\",\"open_event\":\"new_hire\",\"open_event_date\":\"2009-06-01\",\"open_event_ss_open_date\":\"2010-01-06\",\"open_event_ss_close_date\":\"2010-01-16\",\"company\":\"glhhs\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\"}"
        },
        {
            "ee_id": "EX0626295200",
            "type": "census",
            "plan_period_no": 170,
            "data": "{\"employment_status\":\"A\",\"hire_date\":\"2009-06-01\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"55900.00\",\"annual_income\":\"55900.00\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"IT\",\"enrollment_mode\":\"OE\",\"dental_lock_override\":\"N\",\"work_state\":\"U\",\"nationality\":\"US\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"enrollment_status\":\"not_enrolled\",\"enrolled_by\":\"N\\\/A\",\"enrollment_date\":\"N\\\/A\",\"orig_enrollment_date\":\"N\\\/A\",\"has_beneficiaries\":\"Y\",\"has_dependents\":\"Y\",\"is_test_employee\":\"N\",\"overall_eligibility_date\":\"2008-01-01\",\"term_date\":\"\",\"rehire_date\":\"\",\"pay_frequency\":\"\",\"pay_rate\":\"\",\"salaried\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"\",\"division\":\"\",\"service_years\":\"-1\",\"actual_age\":\"49\",\"benefit_age\":\"48\",\"imputed_age\":\"49\",\"pay_schedule\":\"\",\"previous_pay_date\":\"\",\"next_pay_date\":\"\",\"remaining_pay_periods\":\"\",\"now_date\":\"2010-01-06\",\"now_time\":\"08:57\",\"open_event\":\"new_hire\",\"open_event_date\":\"2009-06-01\",\"open_event_ss_open_date\":\"2010-01-06\",\"open_event_ss_close_date\":\"2010-01-16\",\"company\":\"glhhs\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\"}"
        },
        {
            "ee_id": "EX0626295300",
            "type": "census",
            "plan_period_no": 170,
            "data": "{\"employment_status\":\"A\",\"hire_date\":\"2005-05-13\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"24735.50\",\"annual_income\":\"24735.50\",\"pay_periods\":\"52\",\"hours_worked\":\"32\",\"hourly\":\"Y\",\"location\":\"WI\",\"department\":\"S\"}"
        },
        {
            "ee_id": "EX0626295400",
            "type": "census",
            "plan_period_no": 170,
            "data": "{\"enrollment_mode\":\"LE\",\"employment_status\":\"A\",\"hire_date\":\"2009-02-16\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"74999.99\",\"annual_income\":\"74999.99\",\"pay_periods\":\"26\",\"hours_worked\":\"24\",\"hourly\":\"N\",\"location\":\"FL\",\"department\":\"S\",\"dental_lock_override\":\"N\"}"
        },
        {
            "ee_id": "TS0628372400",
            "type": "census",
            "plan_period_no": 2360,
            "data": "{\"employment_status\":\"T\",\"enrollment_mode\":\"OE\",\"hire_date\":\"2012-09-01\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"36999.00\",\"annual_income\":\"36999.00\",\"pay_periods\":\"52\",\"hours_worked\":\"40.0\",\"pay_schedule\":\"pay_schedule_2\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"HR\",\"hs2_code\":\"N\",\"municipality\":\"AK\",\"work_state\":\"U\",\"nationality\":\"US\",\"dental_lock_override\":\"N\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"smoker\":\"N\",\"pay_rate\":\"\",\"weather\":\"N\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"credit_mode\":\"N\",\"rehire_date\":\"2012-02-22\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"enrollment_status\":\"enrolled\",\"enrolled_by\":\"jsmith\",\"enrollment_date\":\"2012-08-29 13:39:25\",\"orig_enrollment_date\":\"2012-08-29 13:39:25\",\"has_beneficiaries\":\"N\",\"has_dependents\":\"Y\",\"is_test_employee\":\"N\",\"overall_eligibility_date\":\"2010-01-01\",\"term_date\":\"2012-09-02\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"service_years\":\"-1\",\"actual_age\":\"30\",\"benefit_age\":\"30\",\"imputed_age\":\"30\",\"pay_schedule_description\":\"\",\"previous_pay_date\":\"\",\"next_pay_date\":\"\",\"remaining_pay_periods\":\"\",\"now_date\":\"2012-08-29\",\"now_time\":\"14:02\",\"open_event\":\"termination\",\"open_event_status\":\"in_process\",\"open_event_date\":\"2012-09-02\",\"open_event_ss_open_date\":\"\",\"open_event_ss_close_date\":\"\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"facility_code\":\"CNE\",\"custom_1\":\"\",\"favorite_pet\":\"N\",\"pension_member_date\":\"\"}"
        },
        {
            "ee_id": "TS0626295200",
            "type": "census",
            "plan_period_no": 2360,
            "data": "{\"employment_status\":\"A\",\"hire_date\":\"1989-07-10\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"55900.00\",\"annual_income\":\"55900.00\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"IT\",\"enrollment_mode\":\"OE\",\"dental_lock_override\":\"N\",\"eye_color\":\"BL\",\"hair_color\":\"N\",\"work_state\":\"U\",\"nationality\":\"US\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"pay_schedule\":\"pay_schedule_1\",\"hs2_code\":\"N\",\"credit_mode\":\"N\",\"salaried\":\"Y\",\"smoker\":\"N\",\"pay_rate\":\"\",\"weather\":\"WIC\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\"}"
        },
        {
            "ee_id": "EX0635705600",
            "type": "census",
            "plan_period_no": 2360,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"credit_mode\":\"N\",\"vb_mode\":\"N\",\"hire_date\":\"2012-09-01\",\"hourly\":\"N\",\"smoker\":\"N\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"40000.00\",\"annual_income\":\"40000.00\",\"pay_rate\":\"\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"location\":\"MI\",\"department\":\"IT\",\"weather\":\"N\",\"favorite_state\":\"U\",\"dental_lock_override\":\"N\",\"custom_2\":\"N\",\"favorite_team\":\"U\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\"}"
        },
        {
            "ee_id": "EX0628372400",
            "type": "census",
            "plan_period_no": 2360,
            "data": "{\"employment_status\":\"T\",\"enrollment_mode\":\"OE\",\"hire_date\":\"2012-09-01\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"36999.00\",\"annual_income\":\"36999.00\",\"pay_periods\":\"52\",\"hours_worked\":\"40.0\",\"pay_schedule\":\"pay_schedule_2\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"HR\",\"hs2_code\":\"N\",\"municipality\":\"AK\",\"work_state\":\"U\",\"nationality\":\"US\",\"dental_lock_override\":\"N\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"smoker\":\"N\",\"pay_rate\":\"\",\"weather\":\"N\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"credit_mode\":\"N\",\"rehire_date\":\"2012-02-22\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"enrollment_status\":\"enrolled\",\"enrolled_by\":\"jsmith\",\"enrollment_date\":\"2012-08-29 13:39:25\",\"orig_enrollment_date\":\"2012-08-29 13:39:25\",\"has_beneficiaries\":\"N\",\"has_dependents\":\"Y\",\"is_test_employee\":\"N\",\"overall_eligibility_date\":\"2010-01-01\",\"term_date\":\"2012-09-02\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"service_years\":\"-1\",\"actual_age\":\"30\",\"benefit_age\":\"30\",\"imputed_age\":\"30\",\"pay_schedule_description\":\"\",\"previous_pay_date\":\"\",\"next_pay_date\":\"\",\"remaining_pay_periods\":\"\",\"now_date\":\"2012-08-29\",\"now_time\":\"14:02\",\"open_event\":\"termination\",\"open_event_status\":\"in_process\",\"open_event_date\":\"2012-09-02\",\"open_event_ss_open_date\":\"\",\"open_event_ss_close_date\":\"\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"facility_code\":\"CNE\",\"custom_1\":\"\",\"favorite_pet\":\"N\",\"pension_member_date\":\"\"}"
        },
        {
            "ee_id": "EX0626295200",
            "type": "census",
            "plan_period_no": 2360,
            "data": "{\"employment_status\":\"A\",\"hire_date\":\"1989-07-10\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"55900.00\",\"annual_income\":\"55900.00\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"IT\",\"enrollment_mode\":\"OE\",\"dental_lock_override\":\"N\",\"eye_color\":\"BL\",\"hair_color\":\"N\",\"work_state\":\"U\",\"nationality\":\"US\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"pay_schedule\":\"pay_schedule_1\",\"hs2_code\":\"N\",\"credit_mode\":\"N\",\"salaried\":\"Y\",\"smoker\":\"N\",\"pay_rate\":\"\",\"weather\":\"WIC\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\"}"
        },
        {
            "ee_id": "EX0626295300",
            "type": "census",
            "plan_period_no": 2360,
            "data": "{\"employment_status\":\"A\",\"hire_date\":\"2005-05-13\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"24735.50\",\"annual_income\":\"24735.50\",\"pay_periods\":\"52\",\"hours_worked\":\"32\",\"hourly\":\"Y\",\"location\":\"WI\",\"department\":\"S\",\"enrollment_mode\":\"OE\",\"pay_schedule\":\"pay_schedule_2\",\"hs2_code\":\"N\",\"work_state\":\"U\",\"nationality\":\"US\",\"dental_lock_override\":\"N\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\"}"
        },
        {
            "ee_id": "EX0626295400",
            "type": "census",
            "plan_period_no": 2360,
            "data": "{\"enrollment_mode\":\"LE\",\"employment_status\":\"A\",\"hire_date\":\"2009-02-16\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"74999.99\",\"annual_income\":\"74999.99\",\"pay_periods\":\"26\",\"hours_worked\":\"24\",\"hourly\":\"N\",\"location\":\"FL\",\"department\":\"S\",\"dental_lock_override\":\"N\",\"pay_schedule\":\"pay_schedule_1\",\"hs2_code\":\"N\",\"work_state\":\"U\",\"nationality\":\"US\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\"}"
        },
        {
            "ee_id": "EX0628370100",
            "type": "census",
            "plan_period_no": 2360,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"hire_date\":\"2002-02-01\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"76525\",\"annual_income\":\"76525\",\"pay_periods\":\"52\",\"hours_worked\":\"40.0\",\"hourly\":\"N\",\"location\":\"WI\",\"department\":\"S\",\"hs2_code\":\"N\",\"municipality\":\"AK\",\"work_state\":\"U\",\"nationality\":\"US\",\"dental_lock_override\":\"N\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"pay_schedule\":\"pay_schedule_2\"}"
        },
        {
            "ee_id": "EX0628372300",
            "type": "census",
            "plan_period_no": 2360,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"hire_date\":\"2006-05-01\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"36799\",\"annual_income\":\"36799\",\"pay_periods\":\"52\",\"hours_worked\":\"40.0\",\"pay_schedule\":\"pay_schedule_2\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"HR\",\"hs2_code\":\"N\",\"municipality\":\"AK\",\"work_state\":\"U\",\"nationality\":\"US\",\"dental_lock_override\":\"N\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\"}"
        },
        {
            "ee_id": "TS0626295200",
            "type": "census",
            "plan_period_no": 3240,
            "data": "{\"employment_status\":\"A\",\"hire_date\":\"1989-07-10\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"55900.00\",\"annual_income\":\"55900.00\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"IT\",\"enrollment_mode\":\"OE\",\"dental_lock_override\":\"N\",\"eye_color\":\"BL\",\"hair_color\":\"N\",\"work_state\":\"U\",\"nationality\":\"US\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"hs2_code\":\"N\",\"credit_mode\":\"N\",\"salaried\":\"Y\",\"smoker\":\"N\",\"pay_rate\":\"\",\"weather\":\"WIC\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"vb_mode\":\"N\",\"pay_schedule\":\"pay_schedule_1\",\"alt_income\":\"\"}"
        },
        {
            "ee_id": "EX0626295200",
            "type": "census",
            "plan_period_no": 3240,
            "data": "{\"employment_status\":\"A\",\"hire_date\":\"1989-07-10\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"55900.00\",\"annual_income\":\"55900.00\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"IT\",\"enrollment_mode\":\"OE\",\"dental_lock_override\":\"N\",\"eye_color\":\"BL\",\"hair_color\":\"N\",\"work_state\":\"U\",\"nationality\":\"US\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"hs2_code\":\"N\",\"credit_mode\":\"N\",\"salaried\":\"Y\",\"smoker\":\"N\",\"pay_rate\":\"\",\"weather\":\"WIC\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"vb_mode\":\"N\",\"pay_schedule\":\"pay_schedule_1\",\"alt_income\":\"\"}"
        },
        {
            "ee_id": "EX0636571700",
            "type": "census",
            "plan_period_no": 3240,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"LE\",\"credit_mode\":\"N\",\"vb_mode\":\"U\",\"hire_date\":\"2013-01-01\",\"hourly\":\"N\",\"smoker\":\"Y\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"500000.00\",\"annual_income\":\"500000.00\",\"pay_rate\":\"\",\"pay_periods\":\"24\",\"pay_schedule\":\"pay_schedule_1\",\"hours_worked\":\"40\",\"location\":\"MI\",\"department\":\"IT\",\"weather\":\"T\",\"favorite_state\":\"MI\",\"dental_lock_override\":\"N\",\"custom_2\":\"N\",\"favorite_team\":\"DL\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"enrollment_status\":\"not_enrolled\",\"enrolled_by\":\"N\\\/A\",\"enrollment_date\":\"N\\\/A\",\"orig_enrollment_date\":\"N\\\/A\",\"has_beneficiaries\":\"N\",\"has_dependents\":\"Y\",\"is_test_employee\":\"N\",\"term_date\":\"\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"rehire_date\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"service_years\":\"0\",\"actual_age\":\"113\",\"benefit_age\":\"113\",\"imputed_age\":\"113\",\"overall_eligibility_date\":\"\",\"pay_schedule_description\":\"2013 Semi-Monthly (24 Pays)\",\"previous_pay_date\":\"02\\\/28\\\/2013\",\"next_pay_date\":\"03\\\/15\\\/2013\",\"remaining_pay_periods\":\"20\",\"now_date\":\"2013-03-04\",\"now_time\":\"13:55\",\"open_event\":\"new_hire\",\"open_event_status\":\"open\",\"open_event_date\":\"2013-01-01\",\"open_event_ss_open_date\":\"2013-03-04\",\"open_event_ss_close_date\":\"2013-03-14\",\"work_state\":\"U\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"favorite_coffee\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"nationality\":\"US\",\"municipality\":\"\",\"hs2_code\":\"N\",\"facility_code\":\"CNE\",\"custom_1\":\"\",\"favorite_pet\":\"N\",\"pension_member_date\":\"\"}"
        },
        {
            "ee_id": "EX0626295400",
            "type": "census",
            "plan_period_no": 3240,
            "data": "{\"enrollment_mode\":\"LE\",\"employment_status\":\"A\",\"hire_date\":\"2009-02-16\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"74999.99\",\"annual_income\":\"74999.99\",\"pay_periods\":\"26\",\"hours_worked\":\"24\",\"hourly\":\"N\",\"location\":\"FL\",\"department\":\"S\",\"dental_lock_override\":\"N\",\"hs2_code\":\"N\",\"work_state\":\"U\",\"nationality\":\"US\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\"}"
        },
        {
            "ee_id": "EX0628372300",
            "type": "census",
            "plan_period_no": 3240,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"hire_date\":\"2006-05-01\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"36799\",\"annual_income\":\"36799\",\"pay_periods\":\"52\",\"hours_worked\":\"40.0\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"HR\",\"hs2_code\":\"N\",\"municipality\":\"AK\",\"work_state\":\"U\",\"nationality\":\"US\",\"dental_lock_override\":\"N\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\"}"
        },
        {
            "ee_id": "EX0628370100",
            "type": "census",
            "plan_period_no": 3240,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"hire_date\":\"2002-02-01\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"76525\",\"annual_income\":\"76525\",\"pay_periods\":\"52\",\"hours_worked\":\"40.0\",\"hourly\":\"N\",\"location\":\"WI\",\"department\":\"S\",\"hs2_code\":\"N\",\"municipality\":\"AK\",\"work_state\":\"U\",\"nationality\":\"US\",\"dental_lock_override\":\"N\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"pay_schedule\":\"pay_schedule_2\"}"
        },
        {
            "ee_id": "EX0635705600",
            "type": "census",
            "plan_period_no": 3240,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"credit_mode\":\"N\",\"vb_mode\":\"N\",\"hire_date\":\"2012-09-01\",\"hourly\":\"N\",\"smoker\":\"N\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"55000.00\",\"annual_income\":\"55000.00\",\"pay_rate\":\"\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"location\":\"MI\",\"department\":\"IT\",\"weather\":\"N\",\"favorite_state\":\"U\",\"dental_lock_override\":\"N\",\"custom_2\":\"N\",\"favorite_team\":\"U\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\"}"
        },
        {
            "ee_id": "TS0641398200",
            "type": "census",
            "plan_period_no": 5330,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"credit_mode\":\"N\",\"vb_mode\":\"N\",\"hire_date\":\"2014-05-05\",\"hourly\":\"N\",\"smoker\":\"N\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"100000\",\"annual_income\":\"100000\",\"alt_income\":\"\",\"pay_rate\":\"\",\"pay_periods\":\"26\",\"pay_schedule\":\"pay_schedule_3\",\"hours_worked\":\"40\",\"location\":\"MI\",\"department\":\"HR\",\"division\":\"29055D\",\"weather\":\"S\",\"favorite_state\":\"MI\",\"dental_lock_override\":\"N\",\"custom_2\":\"N\",\"favorite_team\":\"DT\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\"}"
        },
        {
            "ee_id": "TS0628372400",
            "type": "census",
            "plan_period_no": 5330,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"hire_date\":\"2012-09-01\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"36999.00\",\"annual_income\":\"36999.00\",\"pay_periods\":\"52\",\"hours_worked\":\"40.0\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"HR\",\"hs2_code\":\"N\",\"municipality\":\"AK\",\"work_state\":\"U\",\"nationality\":\"US\",\"dental_lock_override\":\"N\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"smoker\":\"N\",\"pay_rate\":\"\",\"weather\":\"N\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"credit_mode\":\"N\",\"rehire_date\":\"2012-02-22\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"term_date\":\"2012-09-02\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"facility_code\":\"CNE\",\"custom_1\":\"\",\"favorite_pet\":\"N\",\"pension_member_date\":\"\",\"enrollment_status\":\"not_enrolled\",\"enrolled_by\":\"N\\\/A\",\"enrollment_date\":\"N\\\/A\",\"orig_enrollment_date\":\"N\\\/A\",\"has_beneficiaries\":\"N\",\"has_dependents\":\"Y\",\"is_test_employee\":\"N\",\"overall_eligibility_date\":\"2010-01-01\",\"benefit_group\":\"\",\"pay_schedule\":\"pay_schedule_4\",\"pay_schedule_description\":\"52 Pays\",\"pay_schedule_pay_periods\":\"52\",\"previous_pay_date\":\"03\\\/19\\\/2014\",\"next_pay_date\":\"03\\\/26\\\/2014\",\"remaining_pay_periods\":\"40\",\"vb_mode\":\"N\",\"test_field\":\"\",\"alt_income\":\"\",\"open_event\":\"new_hire\",\"open_event_status\":\"open\",\"open_event_date\":\"2012-09-01\",\"open_event_ss_open_date\":\"2014-03-21\",\"open_event_ss_close_date\":\"2014-03-31\",\"benefit_age\":\"31\",\"imputed_age\":\"32\",\"actual_age\":\"32\",\"service_years\":\"1\",\"now_date\":\"2014-03-21\",\"now_time\":\"15:18\"}"
        },
        {
            "ee_id": "TS0626295200",
            "type": "census",
            "plan_period_no": 5330,
            "data": "{\"employment_status\":\"A\",\"hire_date\":\"1989-07-10\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"59342\",\"annual_income\":\"59342\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"IT\",\"enrollment_mode\":\"OE\",\"dental_lock_override\":\"N\",\"work_state\":\"U\",\"nationality\":\"US\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"hs2_code\":\"N\",\"credit_mode\":\"N\",\"smoker\":\"N\",\"pay_rate\":\"\",\"weather\":\"WIC\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"vb_mode\":\"N\",\"enrollment_status\":\"enrolled\",\"enrolled_by\":\"mczmer\",\"enrollment_date\":\"2014-06-24 17:34:51\",\"orig_enrollment_date\":\"2014-03-17 13:46:52\",\"has_beneficiaries\":\"Y\",\"has_dependents\":\"Y\",\"is_test_employee\":\"N\",\"overall_eligibility_date\":\"2008-01-01\",\"term_date\":\"\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"rehire_date\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"benefit_group\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"pay_schedule\":\"pay_schedule_3\",\"pay_schedule_description\":\"26 Pays\",\"pay_schedule_pay_periods\":\"26\",\"previous_pay_date\":\"10\\\/08\\\/2014\",\"next_pay_date\":\"10\\\/22\\\/2014\",\"remaining_pay_periods\":\"5\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"municipality\":\"\",\"facility_code\":\"CNE\",\"custom_1\":\"\",\"favorite_pet\":\"N\",\"pension_member_date\":\"\",\"test_field\":\"\",\"alt_income\":\"\",\"retire_date\":\"\",\"wellness\":\"N\",\"open_event\":\"none\",\"open_event_status\":\"\",\"open_event_date\":\"\",\"open_event_ss_open_date\":\"\",\"open_event_ss_close_date\":\"\",\"open_event_self_directed\":\"N\",\"benefit_age\":\"53\",\"imputed_age\":\"54\",\"actual_age\":\"54\",\"service_years\":\"24\",\"now_date\":\"2014-10-17\",\"now_time\":\"12:08\"}"
        },
        {
            "ee_id": "EX0626295200",
            "type": "census",
            "plan_period_no": 5330,
            "data": "{\"employment_status\":\"A\",\"hire_date\":\"1989-07-10\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"59342\",\"annual_income\":\"59342\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"IT\",\"enrollment_mode\":\"OE\",\"dental_lock_override\":\"N\",\"work_state\":\"U\",\"nationality\":\"US\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"hs2_code\":\"N\",\"credit_mode\":\"N\",\"smoker\":\"N\",\"pay_rate\":\"\",\"weather\":\"WIC\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"vb_mode\":\"N\",\"enrollment_status\":\"enrolled\",\"enrolled_by\":\"mczmer\",\"enrollment_date\":\"2014-06-24 17:34:51\",\"orig_enrollment_date\":\"2014-03-17 13:46:52\",\"has_beneficiaries\":\"Y\",\"has_dependents\":\"Y\",\"is_test_employee\":\"N\",\"overall_eligibility_date\":\"2008-01-01\",\"term_date\":\"\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"rehire_date\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"benefit_group\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"pay_schedule\":\"pay_schedule_3\",\"pay_schedule_description\":\"26 Pays\",\"pay_schedule_pay_periods\":\"26\",\"previous_pay_date\":\"10\\\/08\\\/2014\",\"next_pay_date\":\"10\\\/22\\\/2014\",\"remaining_pay_periods\":\"5\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"municipality\":\"\",\"facility_code\":\"CNE\",\"custom_1\":\"\",\"favorite_pet\":\"N\",\"pension_member_date\":\"\",\"test_field\":\"\",\"alt_income\":\"\",\"retire_date\":\"\",\"wellness\":\"N\",\"open_event\":\"none\",\"open_event_status\":\"\",\"open_event_date\":\"\",\"open_event_ss_open_date\":\"\",\"open_event_ss_close_date\":\"\",\"open_event_self_directed\":\"N\",\"benefit_age\":\"53\",\"imputed_age\":\"54\",\"actual_age\":\"54\",\"service_years\":\"24\",\"now_date\":\"2014-10-17\",\"now_time\":\"12:08\"}"
        },
        {
            "ee_id": "EX0626295400",
            "type": "census",
            "plan_period_no": 5330,
            "data": "{\"enrollment_mode\":\"OE\",\"employment_status\":\"C\",\"hire_date\":\"2009-02-16\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"74999.99\",\"annual_income\":\"74999.99\",\"pay_periods\":\"26\",\"hours_worked\":\"24\",\"hourly\":\"N\",\"location\":\"FL\",\"department\":\"S\",\"dental_lock_override\":\"N\",\"hs2_code\":\"N\",\"work_state\":\"U\",\"nationality\":\"US\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"credit_mode\":\"N\",\"vb_mode\":\"N\",\"smoker\":\"N\",\"alt_income\":\"\",\"pay_rate\":\"\",\"pay_schedule\":\"pay_schedule_3\",\"weather\":\"N\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\"}"
        },
        {
            "ee_id": "EX0641398200",
            "type": "census",
            "plan_period_no": 5330,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"credit_mode\":\"N\",\"vb_mode\":\"N\",\"hire_date\":\"2014-05-05\",\"hourly\":\"N\",\"smoker\":\"N\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"100000\",\"annual_income\":\"100000\",\"alt_income\":\"\",\"pay_rate\":\"\",\"pay_periods\":\"26\",\"pay_schedule\":\"pay_schedule_3\",\"hours_worked\":\"40\",\"location\":\"MI\",\"department\":\"HR\",\"division\":\"29055D\",\"weather\":\"S\",\"favorite_state\":\"MI\",\"dental_lock_override\":\"N\",\"custom_2\":\"N\",\"favorite_team\":\"DT\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\"}"
        },
        {
            "ee_id": "EX0640866400",
            "type": "census",
            "plan_period_no": 5330,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"credit_mode\":\"N\",\"vb_mode\":\"N\",\"hire_date\":\"2014-05-05\",\"hourly\":\"N\",\"smoker\":\"N\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"15000\",\"annual_income\":\"30000\",\"alt_income\":\"\",\"pay_rate\":\"\",\"pay_periods\":\"semiannual\",\"pay_schedule\":\"pay_schedule_3\",\"hours_worked\":\"40+\",\"location\":\"MI\",\"department\":\"IT\",\"division\":\"29055D\",\"weather\":\"N\",\"favorite_state\":\"U\",\"dental_lock_override\":\"N\",\"custom_2\":\"N\",\"favorite_team\":\"U\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\"}"
        },
        {
            "ee_id": "EX0640693900",
            "type": "census",
            "plan_period_no": 5330,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"credit_mode\":\"N\",\"vb_mode\":\"N\",\"hire_date\":\"2014-04-01\",\"hourly\":\"N\",\"smoker\":\"N\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"50000\",\"annual_income\":\"50000\",\"alt_income\":\"\",\"pay_rate\":\"\",\"pay_periods\":\"26\",\"pay_schedule\":\"pay_schedule_3\",\"hours_worked\":\"40\",\"location\":\"MI\",\"department\":\"IT\",\"weather\":\"N\",\"favorite_state\":\"U\",\"dental_lock_override\":\"N\",\"custom_2\":\"N\",\"favorite_team\":\"OM\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\"}"
        },
        {
            "ee_id": "EX0628372300",
            "type": "census",
            "plan_period_no": 5330,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"hire_date\":\"2006-05-01\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"36799.00\",\"annual_income\":\"36799.00\",\"pay_periods\":\"52\",\"hours_worked\":\"40.0\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"HR\",\"hs2_code\":\"N\",\"municipality\":\"AK\",\"work_state\":\"U\",\"nationality\":\"US\",\"dental_lock_override\":\"N\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"enrollment_status\":\"not_enrolled\",\"enrolled_by\":\"N\\\/A\",\"enrollment_date\":\"N\\\/A\",\"orig_enrollment_date\":\"N\\\/A\",\"has_beneficiaries\":\"N\",\"has_dependents\":\"N\",\"is_test_employee\":\"N\",\"overall_eligibility_date\":\"2010-01-01\",\"term_date\":\"\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"rehire_date\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"pay_rate\":\"\",\"smoker\":\"N\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"benefit_group\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"pay_schedule\":\"pay_schedule_4\",\"pay_schedule_description\":\"52 Pays\",\"pay_schedule_pay_periods\":\"52\",\"previous_pay_date\":\"03\\\/19\\\/2014\",\"next_pay_date\":\"03\\\/26\\\/2014\",\"remaining_pay_periods\":\"40\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"facility_code\":\"CNE\",\"custom_2\":\"N\",\"custom_1\":\"\",\"favorite_state\":\"U\",\"favorite_pet\":\"N\",\"weather\":\"N\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"pension_member_date\":\"\",\"credit_mode\":\"N\",\"vb_mode\":\"N\",\"test_field\":\"\",\"alt_income\":\"\",\"open_event\":\"new_hire\",\"open_event_status\":\"open\",\"open_event_date\":\"2006-05-01\",\"open_event_ss_open_date\":\"2014-03-26\",\"open_event_ss_close_date\":\"2014-04-05\",\"benefit_age\":\"29\",\"imputed_age\":\"30\",\"actual_age\":\"29\",\"service_years\":\"7\",\"now_date\":\"2014-03-26\",\"now_time\":\"16:49\"}"
        },
        {
            "ee_id": "EX0628372400",
            "type": "census",
            "plan_period_no": 5330,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"hire_date\":\"2012-09-01\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"36999.00\",\"annual_income\":\"36999.00\",\"pay_periods\":\"52\",\"hours_worked\":\"40.0\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"HR\",\"hs2_code\":\"N\",\"municipality\":\"AK\",\"work_state\":\"U\",\"nationality\":\"US\",\"dental_lock_override\":\"N\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"smoker\":\"N\",\"pay_rate\":\"\",\"weather\":\"N\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"credit_mode\":\"N\",\"rehire_date\":\"2012-02-22\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"term_date\":\"2012-09-02\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"facility_code\":\"CNE\",\"custom_1\":\"\",\"favorite_pet\":\"N\",\"pension_member_date\":\"\",\"enrollment_status\":\"not_enrolled\",\"enrolled_by\":\"N\\\/A\",\"enrollment_date\":\"N\\\/A\",\"orig_enrollment_date\":\"N\\\/A\",\"has_beneficiaries\":\"N\",\"has_dependents\":\"Y\",\"is_test_employee\":\"N\",\"overall_eligibility_date\":\"2010-01-01\",\"benefit_group\":\"\",\"pay_schedule\":\"pay_schedule_4\",\"pay_schedule_description\":\"52 Pays\",\"pay_schedule_pay_periods\":\"52\",\"previous_pay_date\":\"03\\\/19\\\/2014\",\"next_pay_date\":\"03\\\/26\\\/2014\",\"remaining_pay_periods\":\"40\",\"vb_mode\":\"N\",\"test_field\":\"\",\"alt_income\":\"\",\"open_event\":\"new_hire\",\"open_event_status\":\"open\",\"open_event_date\":\"2012-09-01\",\"open_event_ss_open_date\":\"2014-03-21\",\"open_event_ss_close_date\":\"2014-03-31\",\"benefit_age\":\"31\",\"imputed_age\":\"32\",\"actual_age\":\"32\",\"service_years\":\"1\",\"now_date\":\"2014-03-21\",\"now_time\":\"15:18\"}"
        },
        {
            "ee_id": "EX0635705600",
            "type": "census",
            "plan_period_no": 5330,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"credit_mode\":\"N\",\"vb_mode\":\"N\",\"hire_date\":\"2012-09-01\",\"hourly\":\"N\",\"smoker\":\"N\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"55000.00\",\"annual_income\":\"55000.00\",\"pay_rate\":\"\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"location\":\"MI\",\"department\":\"IT\",\"weather\":\"N\",\"favorite_state\":\"U\",\"dental_lock_override\":\"N\",\"custom_2\":\"N\",\"favorite_team\":\"U\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"enrollment_status\":\"not_enrolled\",\"enrolled_by\":\"N\\\/A\",\"enrollment_date\":\"N\\\/A\",\"orig_enrollment_date\":\"N\\\/A\",\"has_beneficiaries\":\"N\",\"has_dependents\":\"N\",\"is_test_employee\":\"N\",\"term_date\":\"\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"rehire_date\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"benefit_group\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"overall_eligibility_date\":\"\",\"pay_schedule\":\"\",\"pay_schedule_description\":\"\",\"pay_schedule_pay_periods\":\"\",\"previous_pay_date\":\"\",\"next_pay_date\":\"\",\"remaining_pay_periods\":\"\",\"work_state\":\"U\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"favorite_coffee\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"nationality\":\"US\",\"municipality\":\"\",\"hs2_code\":\"N\",\"facility_code\":\"CNE\",\"custom_1\":\"\",\"favorite_pet\":\"N\",\"pension_member_date\":\"\",\"test_field\":\"\",\"open_event\":\"new_hire\",\"open_event_status\":\"open\",\"open_event_date\":\"2012-09-01\",\"open_event_ss_open_date\":\"2014-03-05\",\"open_event_ss_close_date\":\"2014-03-15\",\"benefit_age\":\"34\",\"imputed_age\":\"35\",\"actual_age\":\"34\",\"service_years\":\"1\",\"now_date\":\"2014-03-05\",\"now_time\":\"15:23\"}"
        },
        {
            "ee_id": "EX0628370100",
            "type": "census",
            "plan_period_no": 5330,
            "data": "{\"employment_status\":\"T\",\"enrollment_mode\":\"OE\",\"hire_date\":\"2002-02-01\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"76525.00\",\"annual_income\":\"76525.00\",\"pay_periods\":\"52\",\"hours_worked\":\"40.0\",\"hourly\":\"N\",\"location\":\"WI\",\"department\":\"S\",\"hs2_code\":\"N\",\"municipality\":\"AK\",\"work_state\":\"U\",\"nationality\":\"US\",\"dental_lock_override\":\"N\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"credit_mode\":\"N\",\"vb_mode\":\"N\",\"smoker\":\"N\",\"pay_rate\":\"\",\"weather\":\"N\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"enrollment_status\":\"not_enrolled\",\"enrolled_by\":\"N\\\/A\",\"enrollment_date\":\"N\\\/A\",\"orig_enrollment_date\":\"N\\\/A\",\"has_beneficiaries\":\"N\",\"has_dependents\":\"N\",\"is_test_employee\":\"N\",\"overall_eligibility_date\":\"2010-01-01\",\"term_date\":\"2014-02-05\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"rehire_date\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"benefit_group\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"pay_schedule\":\"pay_schedule_4\",\"pay_schedule_description\":\"\",\"pay_schedule_pay_periods\":\"\",\"previous_pay_date\":\"\",\"next_pay_date\":\"\",\"remaining_pay_periods\":\"\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"facility_code\":\"CNE\",\"custom_1\":\"\",\"favorite_pet\":\"N\",\"pension_member_date\":\"\",\"test_field\":\"\",\"open_event\":\"termination\",\"open_event_status\":\"in_process\",\"open_event_date\":\"2014-02-05\",\"open_event_ss_open_date\":\"\",\"open_event_ss_close_date\":\"\",\"benefit_age\":\"29\",\"imputed_age\":\"29\",\"actual_age\":\"29\",\"service_years\":\"12\",\"now_date\":\"2014-02-27\",\"now_time\":\"16:17\",\"alt_income\":\"\"}"
        },
        {
            "ee_id": "TS0641398200",
            "type": "census",
            "plan_period_no": 6590,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"credit_mode\":\"N\",\"vb_mode\":\"N\",\"hire_date\":\"2014-05-05\",\"hourly\":\"N\",\"smoker\":\"N\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"100000\",\"annual_income\":\"100000\",\"alt_income\":\"\",\"pay_rate\":\"\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"location\":\"MI\",\"department\":\"HR\",\"division\":\"29055D\",\"weather\":\"S\",\"favorite_state\":\"MI\",\"dental_lock_override\":\"N\",\"custom_2\":\"N\",\"favorite_team\":\"DT\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\"}"
        },
        {
            "ee_id": "TS0628372400",
            "type": "census",
            "plan_period_no": 6590,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"hire_date\":\"2005-05-13\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"22500\",\"annual_income\":\"22500\",\"pay_periods\":\"12\",\"hours_worked\":\"40.00\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"IT\",\"hs2_code\":\"N\",\"municipality\":\"AK\",\"work_state\":\"U\",\"nationality\":\"US\",\"dental_lock_override\":\"N\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"smoker\":\"N\",\"pay_rate\":\"\",\"weather\":\"N\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"credit_mode\":\"N\",\"rehire_date\":\"2012-02-22\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"term_date\":\"2015-04-15\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"facility_code\":\"CNE\",\"custom_1\":\"\",\"favorite_pet\":\"N\",\"pension_member_date\":\"\",\"benefit_group\":\"\",\"vb_mode\":\"N\",\"test_field\":\"\",\"alt_income\":\"\",\"test_list\":\"A\",\"pay_schedule\":\"pay_schedule_1\",\"certification_status\":\"N\",\"certification_expire_date\":\"2015-01-01\",\"enrollment_status\":\"not_enrolled\",\"enrolled_by\":\"N\\\/A\",\"enrollment_date\":\"N\\\/A\",\"orig_enrollment_date\":\"N\\\/A\",\"has_beneficiaries\":\"N\",\"has_dependents\":\"Y\",\"is_test_employee\":\"N\",\"overall_eligibility_date\":\"2010-01-01\",\"retire_date\":\"\",\"wellness\":\"N\",\"spouse_effective_age\":\"N\\\/A\",\"pay_schedule_description\":\"52 Pays\",\"pay_schedule_pay_periods\":\"52\",\"previous_pay_date\":\"04\\\/11\\\/2015\",\"next_pay_date\":\"04\\\/18\\\/2015\",\"remaining_pay_periods\":\"38\",\"required_doc1_rcvd\":\"Y\",\"required_doc1_rcvd_date\":\"\",\"required_doc2_rcvd\":\"Y\",\"required_doc2_rcvd_date\":\"\",\"required_doc3_rcvd\":\"Y\",\"required_doc3_rcvd_date\":\"\",\"required_doc4_rcvd\":\"Y\",\"required_doc4_rcvd_date\":\"\",\"required_doc5_rcvd\":\"Y\",\"required_doc5_rcvd_date\":\"\",\"pension_date\":\"\",\"open_event\":\"termination\",\"open_event_status\":\"in_process\",\"open_event_date\":\"2015-04-15\",\"open_event_ss_open_date\":\"\",\"open_event_ss_close_date\":\"\",\"open_event_self_directed\":\"N\",\"benefit_age\":\"33\",\"imputed_age\":\"33\",\"actual_age\":\"33\",\"service_years\":\"2\",\"now_date\":\"2015-04-15\",\"now_time\":\"13:33\"}"
        },
        {
            "ee_id": "TS0626295200",
            "type": "census",
            "plan_period_no": 6590,
            "data": "{\"employment_status\":\"A\",\"hire_date\":\"2014-11-18\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"59342.00\",\"annual_income\":\"59342.00\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"IT\",\"enrollment_mode\":\"OE\",\"dental_lock_override\":\"N\",\"work_state\":\"U\",\"nationality\":\"US\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"hs2_code\":\"N\",\"credit_mode\":\"N\",\"smoker\":\"N\",\"pay_rate\":\"\",\"weather\":\"WIC\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"vb_mode\":\"N\",\"term_date\":\"\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"rehire_date\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"benefit_group\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"municipality\":\"\",\"facility_code\":\"CNE\",\"custom_1\":\"\",\"favorite_pet\":\"N\",\"pension_member_date\":\"\",\"test_field\":\"\",\"alt_income\":\"\",\"retire_date\":\"\",\"wellness\":\"N\",\"pay_schedule\":\"pay_schedule_3\"}"
        },
        {
            "ee_id": "EX0650375100",
            "type": "census",
            "plan_period_no": 6590,
            "data": "{\"employment_status\":\"A\",\"test_list\":\"A\",\"enrollment_mode\":\"OE\",\"credit_mode\":\"N\",\"vb_mode\":\"N\",\"hire_date\":\"2015-05-13\",\"hourly\":\"N\",\"smoker\":\"N\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"22500.00\",\"annual_income\":\"22500.00\",\"alt_income\":\"\",\"pay_rate\":\"\",\"pay_periods\":\"12\",\"pay_schedule\":\"pay_schedule_1\",\"hours_worked\":\"40.00\",\"location\":\"MI\",\"department\":\"IT\",\"weather\":\"N\",\"favorite_state\":\"U\",\"dental_lock_override\":\"N\",\"custom_2\":\"N\",\"favorite_team\":\"U\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"certification_status\":\"N\"}"
        },
        {
            "ee_id": "EX0628372400",
            "type": "census",
            "plan_period_no": 6590,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"hire_date\":\"2005-05-13\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"22500\",\"annual_income\":\"22500\",\"pay_periods\":\"12\",\"hours_worked\":\"40.00\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"IT\",\"hs2_code\":\"N\",\"municipality\":\"AK\",\"work_state\":\"U\",\"nationality\":\"US\",\"dental_lock_override\":\"N\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"smoker\":\"N\",\"pay_rate\":\"\",\"weather\":\"N\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"credit_mode\":\"N\",\"rehire_date\":\"2012-02-22\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"term_date\":\"2015-04-15\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"facility_code\":\"CNE\",\"custom_1\":\"\",\"favorite_pet\":\"N\",\"pension_member_date\":\"\",\"benefit_group\":\"\",\"vb_mode\":\"N\",\"test_field\":\"\",\"alt_income\":\"\",\"test_list\":\"A\",\"pay_schedule\":\"pay_schedule_1\",\"certification_status\":\"N\",\"certification_expire_date\":\"2015-01-01\",\"enrollment_status\":\"not_enrolled\",\"enrolled_by\":\"N\\\/A\",\"enrollment_date\":\"N\\\/A\",\"orig_enrollment_date\":\"N\\\/A\",\"has_beneficiaries\":\"N\",\"has_dependents\":\"Y\",\"is_test_employee\":\"N\",\"overall_eligibility_date\":\"2010-01-01\",\"retire_date\":\"\",\"wellness\":\"N\",\"spouse_effective_age\":\"N\\\/A\",\"pay_schedule_description\":\"52 Pays\",\"pay_schedule_pay_periods\":\"52\",\"previous_pay_date\":\"04\\\/11\\\/2015\",\"next_pay_date\":\"04\\\/18\\\/2015\",\"remaining_pay_periods\":\"38\",\"required_doc1_rcvd\":\"Y\",\"required_doc1_rcvd_date\":\"\",\"required_doc2_rcvd\":\"Y\",\"required_doc2_rcvd_date\":\"\",\"required_doc3_rcvd\":\"Y\",\"required_doc3_rcvd_date\":\"\",\"required_doc4_rcvd\":\"Y\",\"required_doc4_rcvd_date\":\"\",\"required_doc5_rcvd\":\"Y\",\"required_doc5_rcvd_date\":\"\",\"pension_date\":\"\",\"open_event\":\"termination\",\"open_event_status\":\"in_process\",\"open_event_date\":\"2015-04-15\",\"open_event_ss_open_date\":\"\",\"open_event_ss_close_date\":\"\",\"open_event_self_directed\":\"N\",\"benefit_age\":\"33\",\"imputed_age\":\"33\",\"actual_age\":\"33\",\"service_years\":\"2\",\"now_date\":\"2015-04-15\",\"now_time\":\"13:33\"}"
        },
        {
            "ee_id": "EX0636571700",
            "type": "census",
            "plan_period_no": 6590,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"LE\",\"credit_mode\":\"N\",\"vb_mode\":\"U\",\"hire_date\":\"2013-01-01\",\"hourly\":\"N\",\"smoker\":\"Y\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"500000.00\",\"annual_income\":\"500000.00\",\"pay_rate\":\"\",\"pay_periods\":\"24\",\"hours_worked\":\"40\",\"location\":\"MI\",\"department\":\"IT\",\"weather\":\"T\",\"favorite_state\":\"MI\",\"dental_lock_override\":\"N\",\"custom_2\":\"N\",\"favorite_team\":\"DL\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"term_date\":\"\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"rehire_date\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"work_state\":\"U\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"favorite_coffee\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"nationality\":\"US\",\"municipality\":\"\",\"hs2_code\":\"N\",\"facility_code\":\"CNE\",\"custom_1\":\"\",\"favorite_pet\":\"N\",\"pension_member_date\":\"\"}"
        },
        {
            "ee_id": "EX0628370100",
            "type": "census",
            "plan_period_no": 6590,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"hire_date\":\"2002-02-01\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"76525.00\",\"annual_income\":\"76525.00\",\"pay_periods\":\"52\",\"hours_worked\":\"40.0\",\"hourly\":\"N\",\"location\":\"WI\",\"department\":\"S\",\"hs2_code\":\"N\",\"municipality\":\"AK\",\"work_state\":\"U\",\"nationality\":\"US\",\"dental_lock_override\":\"N\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"credit_mode\":\"N\",\"vb_mode\":\"N\",\"smoker\":\"N\",\"pay_rate\":\"\",\"weather\":\"N\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"term_date\":\"2014-02-05\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"rehire_date\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"benefit_group\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"facility_code\":\"CNE\",\"custom_1\":\"\",\"favorite_pet\":\"N\",\"pension_member_date\":\"\",\"test_field\":\"\",\"alt_income\":\"\",\"test_list\":\"A\",\"pay_schedule\":\"pay_schedule_4\",\"certification_status\":\"N\"}"
        },
        {
            "ee_id": "EX0640866400",
            "type": "census",
            "plan_period_no": 6590,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"credit_mode\":\"N\",\"vb_mode\":\"U\",\"hire_date\":\"2014-05-05\",\"hourly\":\"N\",\"smoker\":\"N\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"30000.00\",\"annual_income\":\"30000.00\",\"alt_income\":\"\",\"pay_rate\":\"\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"location\":\"MI\",\"department\":\"IT\",\"division\":\"29055D\",\"weather\":\"N\",\"favorite_state\":\"U\",\"dental_lock_override\":\"N\",\"custom_2\":\"N\",\"favorite_team\":\"U\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"test_list\":\"A\",\"pay_schedule\":\"pay_schedule_3\",\"certification_status\":\"RN\",\"certification_expire_date\":\"2015-12-31\"}"
        },
        {
            "ee_id": "EX0626295200",
            "type": "census",
            "plan_period_no": 6590,
            "data": "{\"employment_status\":\"A\",\"hire_date\":\"2014-11-18\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"59342.00\",\"annual_income\":\"59342.00\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"IT\",\"enrollment_mode\":\"OE\",\"dental_lock_override\":\"N\",\"work_state\":\"U\",\"nationality\":\"US\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"hs2_code\":\"N\",\"credit_mode\":\"N\",\"smoker\":\"N\",\"pay_rate\":\"\",\"weather\":\"WIC\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"vb_mode\":\"N\",\"term_date\":\"\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"rehire_date\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"benefit_group\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"municipality\":\"\",\"facility_code\":\"CNE\",\"custom_1\":\"\",\"favorite_pet\":\"N\",\"pension_member_date\":\"\",\"test_field\":\"\",\"alt_income\":\"\",\"retire_date\":\"\",\"wellness\":\"N\",\"pay_schedule\":\"pay_schedule_3\"}"
        },
        {
            "ee_id": "EX0635705600",
            "type": "census",
            "plan_period_no": 6590,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"credit_mode\":\"N\",\"vb_mode\":\"N\",\"hire_date\":\"2012-09-01\",\"hourly\":\"N\",\"smoker\":\"N\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"55000.00\",\"annual_income\":\"55000.00\",\"pay_rate\":\"\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"location\":\"MI\",\"department\":\"IT\",\"weather\":\"N\",\"favorite_state\":\"U\",\"dental_lock_override\":\"N\",\"custom_2\":\"N\",\"favorite_team\":\"U\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"term_date\":\"\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"rehire_date\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"benefit_group\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"work_state\":\"U\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"favorite_coffee\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"nationality\":\"US\",\"municipality\":\"\",\"hs2_code\":\"N\",\"facility_code\":\"CNE\",\"custom_1\":\"\",\"favorite_pet\":\"N\",\"pension_member_date\":\"\",\"test_field\":\"\"}"
        },
        {
            "ee_id": "EX0628372300",
            "type": "census",
            "plan_period_no": 6590,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"hire_date\":\"2006-05-01\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"36799.00\",\"annual_income\":\"36799.00\",\"pay_periods\":\"52\",\"hours_worked\":\"40.0\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"HR\",\"hs2_code\":\"N\",\"municipality\":\"AK\",\"work_state\":\"U\",\"nationality\":\"US\",\"dental_lock_override\":\"N\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"term_date\":\"\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"rehire_date\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"pay_rate\":\"\",\"smoker\":\"N\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"benefit_group\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"facility_code\":\"CNE\",\"custom_2\":\"N\",\"custom_1\":\"\",\"favorite_state\":\"U\",\"favorite_pet\":\"N\",\"weather\":\"N\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"pension_member_date\":\"\",\"credit_mode\":\"N\",\"vb_mode\":\"N\",\"test_field\":\"\",\"alt_income\":\"\"}"
        },
        {
            "ee_id": "EX0640693900",
            "type": "census",
            "plan_period_no": 6590,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"credit_mode\":\"N\",\"vb_mode\":\"N\",\"hire_date\":\"2014-04-01\",\"hourly\":\"N\",\"smoker\":\"N\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"50000\",\"annual_income\":\"50000\",\"alt_income\":\"\",\"pay_rate\":\"\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"location\":\"MI\",\"department\":\"IT\",\"weather\":\"N\",\"favorite_state\":\"U\",\"dental_lock_override\":\"N\",\"custom_2\":\"N\",\"favorite_team\":\"OM\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\"}"
        },
        {
            "ee_id": "EX0641398200",
            "type": "census",
            "plan_period_no": 6590,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"credit_mode\":\"N\",\"vb_mode\":\"N\",\"hire_date\":\"2014-05-05\",\"hourly\":\"N\",\"smoker\":\"N\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"100000\",\"annual_income\":\"100000\",\"alt_income\":\"\",\"pay_rate\":\"\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"location\":\"MI\",\"department\":\"HR\",\"division\":\"29055D\",\"weather\":\"S\",\"favorite_state\":\"MI\",\"dental_lock_override\":\"N\",\"custom_2\":\"N\",\"favorite_team\":\"DT\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\"}"
        },
        {
            "ee_id": "EX0626295400",
            "type": "census",
            "plan_period_no": 6590,
            "data": "{\"enrollment_mode\":\"OE\",\"employment_status\":\"C\",\"hire_date\":\"2009-02-16\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"74999.99\",\"annual_income\":\"74999.99\",\"pay_periods\":\"26\",\"hours_worked\":\"24\",\"hourly\":\"N\",\"location\":\"FL\",\"department\":\"S\",\"dental_lock_override\":\"N\",\"hs2_code\":\"N\",\"work_state\":\"U\",\"nationality\":\"US\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"credit_mode\":\"N\",\"vb_mode\":\"N\",\"smoker\":\"N\",\"alt_income\":\"\",\"pay_rate\":\"\",\"weather\":\"N\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\"}"
        },
        {
            "ee_id": "TS0641398200",
            "type": "census",
            "plan_period_no": 7480,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"credit_mode\":\"N\",\"vb_mode\":\"N\",\"hire_date\":\"2014-05-05\",\"hourly\":\"N\",\"smoker\":\"N\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"100000\",\"annual_income\":\"100000\",\"alt_income\":\"\",\"pay_rate\":\"\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"location\":\"MI\",\"department\":\"HR\",\"division\":\"29055D\",\"weather\":\"S\",\"favorite_state\":\"MI\",\"dental_lock_override\":\"N\",\"custom_2\":\"N\",\"favorite_team\":\"DT\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\"}"
        },
        {
            "ee_id": "TS0626295200",
            "type": "census",
            "plan_period_no": 7480,
            "data": "{\"employment_status\":\"A\",\"hire_date\":\"2014-11-18\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"62425.00\",\"annual_income\":\"62425.00\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"hourly\":\"N\",\"location\":\"WI\",\"department\":\"IT\",\"enrollment_mode\":\"OE\",\"dental_lock_override\":\"N\",\"work_state\":\"U\",\"nationality\":\"US\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"hs2_code\":\"N\",\"credit_mode\":\"N\",\"smoker\":\"N\",\"pay_rate\":\"\",\"weather\":\"WIC\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"vb_mode\":\"N\",\"term_date\":\"\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"rehire_date\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"benefit_group\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"municipality\":\"\",\"facility_code\":\"CNE\",\"custom_1\":\"\",\"favorite_pet\":\"N\",\"pension_member_date\":\"\",\"test_field\":\"\",\"alt_income\":\"\",\"retire_date\":\"\",\"wellness\":\"N\",\"test_list\":\"A\",\"certification_status\":\"N\",\"pay_schedule\":\"pay_schedule_3\"}"
        },
        {
            "ee_id": "EX0626295200",
            "type": "census",
            "plan_period_no": 7480,
            "data": "{\"employment_status\":\"A\",\"hire_date\":\"2014-11-18\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"62425.00\",\"annual_income\":\"62425.00\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"hourly\":\"N\",\"location\":\"WI\",\"department\":\"IT\",\"enrollment_mode\":\"OE\",\"dental_lock_override\":\"N\",\"work_state\":\"U\",\"nationality\":\"US\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"hs2_code\":\"N\",\"credit_mode\":\"N\",\"smoker\":\"N\",\"pay_rate\":\"\",\"weather\":\"WIC\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"vb_mode\":\"N\",\"term_date\":\"\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"rehire_date\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"benefit_group\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"municipality\":\"\",\"facility_code\":\"CNE\",\"custom_1\":\"\",\"favorite_pet\":\"N\",\"pension_member_date\":\"\",\"test_field\":\"\",\"alt_income\":\"\",\"retire_date\":\"\",\"wellness\":\"N\",\"test_list\":\"A\",\"certification_status\":\"N\",\"pay_schedule\":\"pay_schedule_3\"}"
        },
        {
            "ee_id": "EX0655347100",
            "type": "census",
            "plan_period_no": 7480,
            "data": "{\"hourly\":\"N\",\"annual_income\":\"75000\",\"benefit_income\":\"75000\",\"pay_periods\":\"26\",\"pay_schedule\":\"pay_schedule_3\",\"hire_date\":\"2015-03-06\",\"title\":\"Munu EOI Tester\",\"location\":\"MI\",\"department\":\"IT\",\"hours_worked\":\"40\"}"
        },
        {
            "ee_id": "EX0655347000",
            "type": "census",
            "plan_period_no": 7480,
            "data": "{\"hourly\":\"N\",\"annual_income\":\"62425\",\"benefit_income\":\"62425\",\"pay_periods\":\"26\",\"pay_schedule\":\"pay_schedule_3\",\"hire_date\":\"2015-03-07\",\"title\":\"Munu EOI Tester\",\"location\":\"MI\",\"department\":\"IT\",\"hours_worked\":\"40\"}"
        },
        {
            "ee_id": "EX0635705600",
            "type": "census",
            "plan_period_no": 7480,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"credit_mode\":\"N\",\"vb_mode\":\"N\",\"hire_date\":\"2012-09-01\",\"hourly\":\"N\",\"smoker\":\"N\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"55000.00\",\"annual_income\":\"55000.00\",\"pay_rate\":\"\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"location\":\"MI\",\"department\":\"IT\",\"weather\":\"N\",\"favorite_state\":\"U\",\"dental_lock_override\":\"N\",\"custom_2\":\"N\",\"favorite_team\":\"U\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"term_date\":\"\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"rehire_date\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"Munu EOI Tester\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"benefit_group\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"work_state\":\"U\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"favorite_coffee\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"nationality\":\"US\",\"municipality\":\"\",\"hs2_code\":\"N\",\"facility_code\":\"CNE\",\"custom_1\":\"\",\"favorite_pet\":\"N\",\"pension_member_date\":\"\",\"test_field\":\"\",\"test_list\":\"A\",\"alt_income\":\"\",\"pay_schedule\":\"pay_schedule_3\",\"certification_status\":\"N\"}"
        },
        {
            "ee_id": "EX0655347200",
            "type": "census",
            "plan_period_no": 7480,
            "data": "{\"hourly\":\"N\",\"annual_income\":\"22500\",\"benefit_income\":\"22500\",\"pay_periods\":\"26\",\"pay_schedule\":\"pay_schedule_3\",\"hire_date\":\"2015-03-05\",\"title\":\"Munu EOI Tester\",\"location\":\"MI\",\"department\":\"HR\",\"hours_worked\":\"40\"}"
        },
        {
            "ee_id": "EX0655346900",
            "type": "census",
            "plan_period_no": 7480,
            "data": "{\"hourly\":\"N\",\"annual_income\":\"36799\",\"benefit_income\":\"36799\",\"pay_periods\":\"26\",\"pay_schedule\":\"pay_schedule_3\",\"hire_date\":\"2015-03-08\",\"title\":\"Munu EOI Tester\",\"location\":\"MI\",\"department\":\"S\",\"hours_worked\":\"40\"}"
        },
        {
            "ee_id": "EX0655346800",
            "type": "census",
            "plan_period_no": 7480,
            "data": "{\"hourly\":\"N\",\"annual_income\":\"30000\",\"benefit_income\":\"30000\",\"pay_periods\":\"26\",\"pay_schedule\":\"pay_schedule_3\",\"hire_date\":\"2015-03-09\",\"title\":\"Munu EOI Tester\",\"location\":\"MI\",\"department\":\"S\",\"hours_worked\":\"40\"}"
        },
        {
            "ee_id": "EX0655346700",
            "type": "census",
            "plan_period_no": 7480,
            "data": "{\"hourly\":\"N\",\"annual_income\":\"55000\",\"benefit_income\":\"55000\",\"pay_periods\":\"26\",\"pay_schedule\":\"pay_schedule_3\",\"hire_date\":\"2015-03-10\",\"title\":\"Munu EOI Tester\",\"location\":\"MI\",\"department\":\"S\",\"hours_worked\":\"40\"}"
        },
        {
            "ee_id": "EX0655346600",
            "type": "census",
            "plan_period_no": 7480,
            "data": "{\"hourly\":\"N\",\"annual_income\":\"50000.00\",\"benefit_income\":\"50000.00\",\"pay_periods\":\"26\",\"pay_schedule\":\"pay_schedule_3\",\"hire_date\":\"2015-03-11\",\"title\":\"Munu EOI Tester\",\"location\":\"MI\",\"department\":\"HR\",\"hours_worked\":\"40\",\"employment_status\":\"A\",\"test_list\":\"A\",\"enrollment_mode\":\"OE\",\"credit_mode\":\"N\",\"vb_mode\":\"N\",\"smoker\":\"N\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"alt_income\":\"\",\"pay_rate\":\"\",\"weather\":\"N\",\"favorite_state\":\"U\",\"dental_lock_override\":\"N\",\"custom_2\":\"N\",\"favorite_team\":\"U\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"certification_status\":\"N\"}"
        },
        {
            "ee_id": "EX0655346500",
            "type": "census",
            "plan_period_no": 7480,
            "data": "{\"hourly\":\"N\",\"annual_income\":\"40000\",\"benefit_income\":\"40000\",\"pay_periods\":\"26\",\"pay_schedule\":\"pay_schedule_3\",\"hire_date\":\"2015-03-12\",\"title\":\"Munu EOI Tester\",\"location\":\"MI\",\"department\":\"MFG\",\"hours_worked\":\"40\"}"
        },
        {
            "ee_id": "EX0655346400",
            "type": "census",
            "plan_period_no": 7480,
            "data": "{\"hourly\":\"N\",\"annual_income\":\"100000\",\"benefit_income\":\"100000\",\"pay_periods\":\"26\",\"pay_schedule\":\"pay_schedule_3\",\"hire_date\":\"2015-03-13\",\"title\":\"Munu EOI Tester\",\"location\":\"MI\",\"department\":\"MFG\",\"hours_worked\":\"40\"}"
        },
        {
            "ee_id": "EX0655346300",
            "type": "census",
            "plan_period_no": 7480,
            "data": "{\"hourly\":\"N\",\"annual_income\":\"76525\",\"benefit_income\":\"76525\",\"pay_periods\":\"26\",\"pay_schedule\":\"pay_schedule_3\",\"hire_date\":\"2015-03-14\",\"title\":\"Munu EOI Tester\",\"location\":\"MI\",\"department\":\"MFG\",\"hours_worked\":\"40\"}"
        },
        {
            "ee_id": "EX0650375100",
            "type": "census",
            "plan_period_no": 7480,
            "data": "{\"employment_status\":\"A\",\"test_list\":\"A\",\"enrollment_mode\":\"OE\",\"credit_mode\":\"N\",\"vb_mode\":\"N\",\"hire_date\":\"2015-05-13\",\"hourly\":\"N\",\"smoker\":\"N\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"22500.00\",\"annual_income\":\"22500.00\",\"alt_income\":\"\",\"pay_rate\":\"\",\"pay_periods\":\"12\",\"pay_schedule\":\"pay_schedule_1\",\"hours_worked\":\"40.00\",\"location\":\"MI\",\"department\":\"IT\",\"weather\":\"N\",\"favorite_state\":\"U\",\"dental_lock_override\":\"N\",\"custom_2\":\"N\",\"favorite_team\":\"U\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"certification_status\":\"N\"}"
        },
        {
            "ee_id": "EX0636571700",
            "type": "census",
            "plan_period_no": 7480,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"LE\",\"credit_mode\":\"N\",\"vb_mode\":\"U\",\"hire_date\":\"2013-01-01\",\"hourly\":\"N\",\"smoker\":\"Y\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"500000.00\",\"annual_income\":\"500000.00\",\"pay_rate\":\"\",\"pay_periods\":\"12\",\"hours_worked\":\"40\",\"location\":\"MI\",\"department\":\"IT\",\"weather\":\"T\",\"favorite_state\":\"MI\",\"dental_lock_override\":\"N\",\"custom_2\":\"N\",\"favorite_team\":\"DL\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"term_date\":\"\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"rehire_date\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"work_state\":\"U\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"favorite_coffee\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"nationality\":\"US\",\"municipality\":\"\",\"hs2_code\":\"N\",\"facility_code\":\"CNE\",\"custom_1\":\"\",\"favorite_pet\":\"N\",\"pension_member_date\":\"\",\"test_list\":\"A\",\"alt_income\":\"\",\"pay_schedule\":\"pay_schedule_1\",\"certification_status\":\"N\"}"
        },
        {
            "ee_id": "EX0626295400",
            "type": "census",
            "plan_period_no": 7480,
            "data": "{\"enrollment_mode\":\"OE\",\"employment_status\":\"C\",\"hire_date\":\"2009-02-16\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"74999.99\",\"annual_income\":\"74999.99\",\"pay_periods\":\"26\",\"hours_worked\":\"24\",\"hourly\":\"N\",\"location\":\"FL\",\"department\":\"S\",\"dental_lock_override\":\"N\",\"hs2_code\":\"N\",\"work_state\":\"U\",\"nationality\":\"US\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"credit_mode\":\"N\",\"vb_mode\":\"N\",\"smoker\":\"N\",\"alt_income\":\"\",\"pay_rate\":\"\",\"weather\":\"N\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\"}"
        },
        {
            "ee_id": "EX0641398200",
            "type": "census",
            "plan_period_no": 7480,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"credit_mode\":\"N\",\"vb_mode\":\"N\",\"hire_date\":\"2014-05-05\",\"hourly\":\"N\",\"smoker\":\"N\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"100000\",\"annual_income\":\"100000\",\"alt_income\":\"\",\"pay_rate\":\"\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"location\":\"MI\",\"department\":\"HR\",\"division\":\"29055D\",\"weather\":\"S\",\"favorite_state\":\"MI\",\"dental_lock_override\":\"N\",\"custom_2\":\"N\",\"favorite_team\":\"DT\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\"}"
        },
        {
            "ee_id": "EX0640693900",
            "type": "census",
            "plan_period_no": 7480,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"credit_mode\":\"N\",\"vb_mode\":\"N\",\"hire_date\":\"2014-04-01\",\"hourly\":\"N\",\"smoker\":\"N\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"50000\",\"annual_income\":\"50000\",\"alt_income\":\"\",\"pay_rate\":\"\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"location\":\"MI\",\"department\":\"IT\",\"weather\":\"N\",\"favorite_state\":\"U\",\"dental_lock_override\":\"N\",\"custom_2\":\"N\",\"favorite_team\":\"OM\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\"}"
        },
        {
            "ee_id": "EX0628372300",
            "type": "census",
            "plan_period_no": 7480,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"hire_date\":\"2006-05-01\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"36799.00\",\"annual_income\":\"36799.00\",\"pay_periods\":\"52\",\"hours_worked\":\"40.0\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"HR\",\"hs2_code\":\"N\",\"municipality\":\"AK\",\"work_state\":\"U\",\"nationality\":\"US\",\"dental_lock_override\":\"N\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"term_date\":\"\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"rehire_date\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"pay_rate\":\"\",\"smoker\":\"N\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"benefit_group\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"facility_code\":\"CNE\",\"custom_2\":\"N\",\"custom_1\":\"\",\"favorite_state\":\"U\",\"favorite_pet\":\"N\",\"weather\":\"N\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"pension_member_date\":\"\",\"credit_mode\":\"N\",\"vb_mode\":\"N\",\"test_field\":\"\",\"alt_income\":\"\"}"
        },
        {
            "ee_id": "EX0640866400",
            "type": "census",
            "plan_period_no": 7480,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"credit_mode\":\"N\",\"vb_mode\":\"U\",\"hire_date\":\"2014-05-05\",\"hourly\":\"N\",\"smoker\":\"N\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"30000.00\",\"annual_income\":\"30000.00\",\"alt_income\":\"\",\"pay_rate\":\"\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"location\":\"MI\",\"department\":\"IT\",\"division\":\"29055D\",\"weather\":\"N\",\"favorite_state\":\"U\",\"dental_lock_override\":\"N\",\"custom_2\":\"N\",\"favorite_team\":\"U\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"test_list\":\"A\",\"certification_status\":\"RN\",\"certification_expire_date\":\"2015-12-31\"}"
        },
        {
            "ee_id": "EX0628370100",
            "type": "census",
            "plan_period_no": 7480,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"hire_date\":\"2002-02-01\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"76525.00\",\"annual_income\":\"76525.00\",\"pay_periods\":\"52\",\"hours_worked\":\"40.0\",\"hourly\":\"N\",\"location\":\"WI\",\"department\":\"S\",\"hs2_code\":\"N\",\"municipality\":\"AK\",\"work_state\":\"U\",\"nationality\":\"US\",\"dental_lock_override\":\"N\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"credit_mode\":\"N\",\"vb_mode\":\"N\",\"smoker\":\"N\",\"pay_rate\":\"\",\"weather\":\"N\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"term_date\":\"2014-02-05\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"rehire_date\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"benefit_group\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"facility_code\":\"CNE\",\"custom_1\":\"\",\"favorite_pet\":\"N\",\"pension_member_date\":\"\",\"test_field\":\"\",\"alt_income\":\"\",\"test_list\":\"A\",\"certification_status\":\"N\"}"
        },
        {
            "ee_id": "TS0628372400",
            "type": "census",
            "plan_period_no": 9640,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"hire_date\":\"2005-05-13\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"22500.00\",\"annual_income\":\"22500.00\",\"pay_periods\":\"12\",\"hours_worked\":\"40.00\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"IT\",\"hs2_code\":\"N\",\"municipality\":\"AK\",\"work_state\":\"U\",\"nationality\":\"US\",\"dental_lock_override\":\"N\",\"favorite_coffee\":\"U\",\"favorite_team\":\"DT\",\"smoker\":\"N\",\"pay_rate\":\"\",\"weather\":\"W\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"credit_mode\":\"N\",\"rehire_date\":\"2012-02-22\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"term_date\":\"2015-04-15\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"Software Engineer I\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"facility_code\":\"CNE\",\"custom_1\":\"\",\"favorite_pet\":\"N\",\"pension_member_date\":\"\",\"benefit_group\":\"\",\"vb_mode\":\"N\",\"test_field\":\"\",\"alt_income\":\"\",\"test_list\":\"A\",\"certification_status\":\"N\",\"certification_expire_date\":\"2015-01-01\",\"retire_date\":\"\",\"wellness\":\"N\",\"spouse_effective_age\":\"N\\\/A\",\"required_doc1_rcvd\":\"Y\",\"required_doc1_rcvd_date\":\"\",\"required_doc2_rcvd\":\"Y\",\"required_doc2_rcvd_date\":\"\",\"required_doc3_rcvd\":\"Y\",\"required_doc3_rcvd_date\":\"\",\"required_doc4_rcvd\":\"Y\",\"required_doc4_rcvd_date\":\"\",\"required_doc5_rcvd\":\"Y\",\"required_doc5_rcvd_date\":\"\",\"pension_date\":\"\",\"eofi_mode\":\"R\",\"pay_schedule\":\"pay_schedule_1\"}"
        },
        {
            "ee_id": "TS0626295200",
            "type": "census",
            "plan_period_no": 9640,
            "data": "{\"employment_status\":\"A\",\"hire_date\":\"2014-11-18\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"64825.00\",\"annual_income\":\"64825.00\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"hourly\":\"N\",\"location\":\"WI\",\"department\":\"IT\",\"enrollment_mode\":\"OE\",\"dental_lock_override\":\"N\",\"work_state\":\"U\",\"nationality\":\"US\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"hs2_code\":\"N\",\"credit_mode\":\"N\",\"smoker\":\"N\",\"pay_rate\":\"\",\"weather\":\"WIC\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"vb_mode\":\"N\",\"term_date\":\"\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"rehire_date\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"benefit_group\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"municipality\":\"\",\"facility_code\":\"CNE\",\"custom_1\":\"\",\"favorite_pet\":\"N\",\"pension_member_date\":\"\",\"test_field\":\"\",\"alt_income\":\"\",\"retire_date\":\"\",\"wellness\":\"N\",\"test_list\":\"A\",\"certification_status\":\"N\",\"eofi_mode\":\"R\",\"pay_schedule\":\"pay_schedule_3\"}"
        },
        {
            "ee_id": "EX0628372400",
            "type": "census",
            "plan_period_no": 9640,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"hire_date\":\"2005-05-13\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"22500.00\",\"annual_income\":\"22500.00\",\"pay_periods\":\"12\",\"hours_worked\":\"40.00\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"IT\",\"hs2_code\":\"N\",\"municipality\":\"AK\",\"work_state\":\"U\",\"nationality\":\"US\",\"dental_lock_override\":\"N\",\"favorite_coffee\":\"U\",\"favorite_team\":\"DT\",\"smoker\":\"N\",\"pay_rate\":\"\",\"weather\":\"W\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"credit_mode\":\"N\",\"rehire_date\":\"2012-02-22\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"term_date\":\"2015-04-15\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"Software Engineer I\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"facility_code\":\"CNE\",\"custom_1\":\"\",\"favorite_pet\":\"N\",\"pension_member_date\":\"\",\"benefit_group\":\"\",\"vb_mode\":\"N\",\"test_field\":\"\",\"alt_income\":\"\",\"test_list\":\"A\",\"certification_status\":\"N\",\"certification_expire_date\":\"2015-01-01\",\"retire_date\":\"\",\"wellness\":\"N\",\"spouse_effective_age\":\"N\\\/A\",\"required_doc1_rcvd\":\"Y\",\"required_doc1_rcvd_date\":\"\",\"required_doc2_rcvd\":\"Y\",\"required_doc2_rcvd_date\":\"\",\"required_doc3_rcvd\":\"Y\",\"required_doc3_rcvd_date\":\"\",\"required_doc4_rcvd\":\"Y\",\"required_doc4_rcvd_date\":\"\",\"required_doc5_rcvd\":\"Y\",\"required_doc5_rcvd_date\":\"\",\"pension_date\":\"\",\"eofi_mode\":\"R\",\"pay_schedule\":\"pay_schedule_1\"}"
        },
        {
            "ee_id": "EX0626295200",
            "type": "census",
            "plan_period_no": 9640,
            "data": "{\"employment_status\":\"A\",\"hire_date\":\"2014-11-18\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"64825.00\",\"annual_income\":\"64825.00\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"hourly\":\"N\",\"location\":\"WI\",\"department\":\"IT\",\"enrollment_mode\":\"OE\",\"dental_lock_override\":\"N\",\"work_state\":\"U\",\"nationality\":\"US\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"hs2_code\":\"N\",\"credit_mode\":\"N\",\"smoker\":\"N\",\"pay_rate\":\"\",\"weather\":\"WIC\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"vb_mode\":\"N\",\"term_date\":\"\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"rehire_date\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"benefit_group\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"municipality\":\"\",\"facility_code\":\"CNE\",\"custom_1\":\"\",\"favorite_pet\":\"N\",\"pension_member_date\":\"\",\"test_field\":\"\",\"alt_income\":\"\",\"retire_date\":\"\",\"wellness\":\"N\",\"test_list\":\"A\",\"certification_status\":\"N\",\"eofi_mode\":\"R\",\"pay_schedule\":\"pay_schedule_3\"}"
        },
        {
            "ee_id": "EX0640866400",
            "type": "census",
            "plan_period_no": 9640,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"credit_mode\":\"N\",\"vb_mode\":\"U\",\"hire_date\":\"2014-05-05\",\"hourly\":\"N\",\"smoker\":\"N\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"30000.00\",\"annual_income\":\"30000.00\",\"alt_income\":\"\",\"pay_rate\":\"\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"location\":\"MI\",\"department\":\"IT\",\"division\":\"29055D\",\"weather\":\"N\",\"favorite_state\":\"U\",\"dental_lock_override\":\"N\",\"custom_2\":\"N\",\"favorite_team\":\"U\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"test_list\":\"A\",\"certification_status\":\"RN\",\"certification_expire_date\":\"2015-12-31\"}"
        },
        {
            "ee_id": "EX0655346600",
            "type": "census",
            "plan_period_no": 9640,
            "data": "{\"hourly\":\"N\",\"annual_income\":\"50000.00\",\"benefit_income\":\"50000.00\",\"pay_periods\":\"26\",\"hire_date\":\"2015-03-11\",\"title\":\"Munu EOI Tester\",\"location\":\"MI\",\"department\":\"HR\",\"hours_worked\":\"40\",\"employment_status\":\"A\",\"test_list\":\"A\",\"enrollment_mode\":\"OE\",\"credit_mode\":\"N\",\"vb_mode\":\"N\",\"smoker\":\"N\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"alt_income\":\"\",\"pay_rate\":\"\",\"weather\":\"N\",\"favorite_state\":\"U\",\"dental_lock_override\":\"N\",\"custom_2\":\"N\",\"favorite_team\":\"U\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"certification_status\":\"N\"}"
        },
        {
            "ee_id": "TS0628372400",
            "type": "census",
            "plan_period_no": 11620,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"hire_date\":\"2005-05-13\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"22500.00\",\"annual_income\":\"22500.00\",\"pay_periods\":\"12\",\"hours_worked\":\"40.00\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"IT\",\"hs2_code\":\"N\",\"municipality\":\"AK\",\"work_state\":\"U\",\"nationality\":\"US\",\"dental_lock_override\":\"N\",\"favorite_coffee\":\"U\",\"favorite_team\":\"DT\",\"smoker\":\"N\",\"pay_rate\":\"\",\"weather\":\"S\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"credit_mode\":\"N\",\"rehire_date\":\"2012-02-22\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"term_date\":\"2015-04-15\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"Software Engineer I\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"facility_code\":\"CNE\",\"custom_1\":\"\",\"favorite_pet\":\"N\",\"pension_member_date\":\"\",\"benefit_group\":\"\",\"vb_mode\":\"N\",\"test_field\":\"\",\"alt_income\":\"\",\"test_list\":\"A\",\"certification_status\":\"N\",\"certification_expire_date\":\"2015-01-01\",\"retire_date\":\"\",\"wellness\":\"N\",\"spouse_effective_age\":\"N\\\/A\",\"required_doc1_rcvd\":\"Y\",\"required_doc1_rcvd_date\":\"\",\"required_doc2_rcvd\":\"Y\",\"required_doc2_rcvd_date\":\"\",\"required_doc3_rcvd\":\"Y\",\"required_doc3_rcvd_date\":\"\",\"required_doc4_rcvd\":\"Y\",\"required_doc4_rcvd_date\":\"\",\"required_doc5_rcvd\":\"Y\",\"required_doc5_rcvd_date\":\"\",\"pension_date\":\"\",\"eofi_mode\":\"R\",\"pay_schedule\":\"pay_schedule_1\"}"
        },
        {
            "ee_id": "TS0626295200",
            "type": "census",
            "plan_period_no": 11620,
            "data": "{\"employment_status\":\"A\",\"hire_date\":\"2014-11-18\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"64825.00\",\"annual_income\":\"64825.00\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"hourly\":\"N\",\"location\":\"WI\",\"department\":\"IT\",\"enrollment_mode\":\"OE\",\"dental_lock_override\":\"N\",\"work_state\":\"U\",\"nationality\":\"US\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"hs2_code\":\"N\",\"credit_mode\":\"N\",\"smoker\":\"N\",\"pay_rate\":\"\",\"weather\":\"WIC\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"vb_mode\":\"N\",\"term_date\":\"\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"rehire_date\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"benefit_group\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"municipality\":\"\",\"facility_code\":\"CNE\",\"custom_1\":\"\",\"favorite_pet\":\"N\",\"pension_member_date\":\"\",\"test_field\":\"\",\"alt_income\":\"\",\"retire_date\":\"\",\"wellness\":\"N\",\"test_list\":\"A\",\"certification_status\":\"N\",\"eofi_mode\":\"R\",\"pay_schedule\":\"pay_schedule_3\"}"
        },
        {
            "ee_id": "TS0641398200",
            "type": "census",
            "plan_period_no": 11620,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"credit_mode\":\"N\",\"vb_mode\":\"N\",\"hire_date\":\"2014-05-05\",\"hourly\":\"N\",\"smoker\":\"N\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"100000.00\",\"annual_income\":\"100000.00\",\"alt_income\":\"\",\"pay_rate\":\"\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"location\":\"MI\",\"department\":\"HR\",\"division\":\"29055D\",\"weather\":\"S\",\"favorite_state\":\"MI\",\"dental_lock_override\":\"N\",\"custom_2\":\"N\",\"favorite_team\":\"DT\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"eofi_mode\":\"R\",\"pay_schedule\":\"pay_schedule_3\",\"certification_status\":\"N\"}"
        },
        {
            "ee_id": "EX0628372400",
            "type": "census",
            "plan_period_no": 11620,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"hire_date\":\"2005-05-13\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"22500.00\",\"annual_income\":\"22500.00\",\"pay_periods\":\"12\",\"hours_worked\":\"40.00\",\"hourly\":\"N\",\"location\":\"MI\",\"department\":\"IT\",\"hs2_code\":\"N\",\"municipality\":\"AK\",\"work_state\":\"U\",\"nationality\":\"US\",\"dental_lock_override\":\"N\",\"favorite_coffee\":\"U\",\"favorite_team\":\"DT\",\"smoker\":\"N\",\"pay_rate\":\"\",\"weather\":\"S\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"credit_mode\":\"N\",\"rehire_date\":\"2012-02-22\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"term_date\":\"2015-04-15\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"Software Engineer I\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"facility_code\":\"CNE\",\"custom_1\":\"\",\"favorite_pet\":\"N\",\"pension_member_date\":\"\",\"benefit_group\":\"\",\"vb_mode\":\"N\",\"test_field\":\"\",\"alt_income\":\"\",\"test_list\":\"A\",\"certification_status\":\"N\",\"certification_expire_date\":\"2015-01-01\",\"retire_date\":\"\",\"wellness\":\"N\",\"spouse_effective_age\":\"N\\\/A\",\"required_doc1_rcvd\":\"Y\",\"required_doc1_rcvd_date\":\"\",\"required_doc2_rcvd\":\"Y\",\"required_doc2_rcvd_date\":\"\",\"required_doc3_rcvd\":\"Y\",\"required_doc3_rcvd_date\":\"\",\"required_doc4_rcvd\":\"Y\",\"required_doc4_rcvd_date\":\"\",\"required_doc5_rcvd\":\"Y\",\"required_doc5_rcvd_date\":\"\",\"pension_date\":\"\",\"eofi_mode\":\"R\",\"pay_schedule\":\"pay_schedule_1\"}"
        },
        {
            "ee_id": "EX0641398200",
            "type": "census",
            "plan_period_no": 11620,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"credit_mode\":\"N\",\"vb_mode\":\"N\",\"hire_date\":\"2014-05-05\",\"hourly\":\"N\",\"smoker\":\"N\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"100000.00\",\"annual_income\":\"100000.00\",\"alt_income\":\"\",\"pay_rate\":\"\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"location\":\"MI\",\"department\":\"HR\",\"division\":\"29055D\",\"weather\":\"S\",\"favorite_state\":\"MI\",\"dental_lock_override\":\"N\",\"custom_2\":\"N\",\"favorite_team\":\"DT\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"eofi_mode\":\"R\",\"pay_schedule\":\"pay_schedule_3\",\"certification_status\":\"N\"}"
        },
        {
            "ee_id": "EX0626295200",
            "type": "census",
            "plan_period_no": 11620,
            "data": "{\"employment_status\":\"A\",\"hire_date\":\"2014-11-18\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"64825.00\",\"annual_income\":\"64825.00\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"hourly\":\"N\",\"location\":\"WI\",\"department\":\"IT\",\"enrollment_mode\":\"OE\",\"dental_lock_override\":\"N\",\"work_state\":\"U\",\"nationality\":\"US\",\"favorite_coffee\":\"U\",\"favorite_team\":\"U\",\"hs2_code\":\"N\",\"credit_mode\":\"N\",\"smoker\":\"N\",\"pay_rate\":\"\",\"weather\":\"WIC\",\"favorite_state\":\"U\",\"custom_2\":\"N\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"vb_mode\":\"N\",\"term_date\":\"\",\"benefit_eligibility_date\":\"\",\"cobra_reason\":\"\",\"maintenance_reason_834\":\"\",\"rehire_date\":\"\",\"salary_change_date\":\"\",\"pay_frequency\":\"\",\"key_employee\":\"Y\",\"marital_status\":\"\",\"fulltime\":\"Y\",\"union\":\"N\",\"exempt\":\"Y\",\"title\":\"\",\"company\":\"glhhs\",\"division\":\"\",\"ee_class\":\"\",\"ee_type\":\"\",\"benefit_group\":\"\",\"plan\":\"\",\"spouse_employee\":\"\",\"override_field_1\":\"N\",\"cost_center\":\"HQ\",\"other_favorite_team\":\"U\",\"weather_today\":\"U\",\"is_it_sunny\":\"U\",\"humidity_index\":\"U\",\"favorite_hockey_team\":\"U\",\"municipality\":\"\",\"facility_code\":\"CNE\",\"custom_1\":\"\",\"favorite_pet\":\"N\",\"pension_member_date\":\"\",\"test_field\":\"\",\"alt_income\":\"\",\"retire_date\":\"\",\"wellness\":\"N\",\"test_list\":\"A\",\"certification_status\":\"N\",\"eofi_mode\":\"R\",\"pay_schedule\":\"pay_schedule_3\"}"
        },
        {
            "ee_id": "EX0655346600",
            "type": "census",
            "plan_period_no": 11620,
            "data": "{\"hourly\":\"N\",\"annual_income\":\"50000.00\",\"benefit_income\":\"50000.00\",\"pay_periods\":\"26\",\"hire_date\":\"2015-03-11\",\"title\":\"Munu EOI Tester\",\"location\":\"MI\",\"department\":\"HR\",\"hours_worked\":\"40\",\"employment_status\":\"A\",\"test_list\":\"A\",\"enrollment_mode\":\"OE\",\"credit_mode\":\"N\",\"vb_mode\":\"N\",\"smoker\":\"N\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"alt_income\":\"\",\"pay_rate\":\"\",\"weather\":\"N\",\"favorite_state\":\"U\",\"dental_lock_override\":\"N\",\"custom_2\":\"N\",\"favorite_team\":\"U\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"certification_status\":\"N\",\"eofi_mode\":\"R\",\"pay_schedule\":\"pay_schedule_3\"}"
        },
        {
            "ee_id": "EX0668484500",
            "type": "census",
            "plan_period_no": 11620,
            "data": "{\"employment_status\":\"A\",\"test_list\":\"A\",\"enrollment_mode\":\"LE\",\"credit_mode\":\"N\",\"eofi_mode\":\"R\",\"vb_mode\":\"N\",\"hire_date\":\"2018-03-20\",\"hourly\":\"N\",\"smoker\":\"N\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"9999.99\",\"annual_income\":\"9999.99\",\"alt_income\":\"\",\"pay_rate\":\"\",\"pay_periods\":\"24\",\"pay_schedule\":\"pay_schedule_2\",\"hours_worked\":\"40\",\"location\":\"MI\",\"department\":\"HR\",\"weather\":\"N\",\"favorite_state\":\"U\",\"dental_lock_override\":\"N\",\"custom_2\":\"N\",\"favorite_team\":\"U\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"certification_status\":\"N\"}"
        },
        {
            "ee_id": "EX0640866400",
            "type": "census",
            "plan_period_no": 11620,
            "data": "{\"employment_status\":\"A\",\"enrollment_mode\":\"OE\",\"credit_mode\":\"N\",\"vb_mode\":\"U\",\"hire_date\":\"2014-05-05\",\"hourly\":\"N\",\"smoker\":\"N\",\"management\":\"N\",\"hsa_er_contribution\":\"Y\",\"benefit_income\":\"30000.00\",\"annual_income\":\"30000.00\",\"alt_income\":\"\",\"pay_rate\":\"\",\"pay_periods\":\"26\",\"hours_worked\":\"40\",\"location\":\"MI\",\"department\":\"IT\",\"division\":\"29055D\",\"weather\":\"N\",\"favorite_state\":\"U\",\"dental_lock_override\":\"N\",\"custom_2\":\"N\",\"favorite_team\":\"U\",\"pension_cola\":\"N\",\"pension_open_bal\":\"\",\"test_list\":\"A\",\"certification_status\":\"RN\",\"certification_expire_date\":\"2015-12-31\"}"
        }
    ],
*/
    "employee_elections": [
        {
            "ee_id": "EX0628372400",
            "ben_code": "med",
            "option_code": "single",
            "tier_code": "ppo_2",
            "plan_period_no": 11620
        },
        {
            "ee_id": "EX0628372400",
            "ben_code": "vis",
            "option_code": "single",
            "tier_code": "coverage",
            "plan_period_no": 11620
        },
        {
            "ee_id": "EX0628372400",
            "ben_code": "den",
            "option_code": "single",
            "tier_code": "coverage",
            "plan_period_no": 11620
        }
    ],

    "pricetags": [
        {
            "plan_period_no": 11620,
            "code": "11620__ee__per_pay",
            "type_code": "ee",
            "type_label": "Employee",
            "frequency_code": "per_pay",
            "frequency_label": "Per Pay"
        },
        {
            "plan_period_no": 11620,
            "code": "11620__er__per_pay",
            "type_code": "ee",
            "type_label": "Employer",
            "frequency_code": "per_pay",
            "frequency_label": "Per Pay"
        }
    ],

    "benefits": [
        {
            "plan_period_no": 11620,
            "code": "ben",
            "name": "Medical",

            "tiers": [
                {
                    "code": "single",
                    "name": "Single"
                },
                {
                    "code": "family",
                    "name": "Family"
                }
            ],

            "options": [
                {
                    "code": "ppo_1",
                    "name": "Basic Medical",
                    "carrier_code": "bcbs_mi"
                },
                {
                    "code": "ppo_2",
                    "name": "Premium Medical",
                    "carrier_code": "bcbs_mi"
                },
                {
                    "code": "waive_coverage",
                    "name": "Waive Coverage",
                    "carrier_code": "_none"
                }
            ],

            "prices": [
                {
                    "plan_period_no": 11620,
                    "option_code": "ppo_1",
                    "tier_code": "single",
                    "amount": 10.00
                },
                {
                    "plan_period_no": 11620,
                    "option_code": "ppo_1",
                    "tier_code": "family",
                    "amount": 20.00
                },
                {
                    "plan_period_no": 11620,
                    "option_code": "ppo_2",
                    "tier_code": "single",
                    "amount": 11.01
                },
                {
                    "plan_period_no": 11620,
                    "option_code": "ppo_2",
                    "tier_code": "family",
                    "amount": 22.02
                }
            ]
        },
        {
            "plan_period_no": 11620,
            "code": "vis",
            "name": "Vision",

            "tiers": [
                {
                    "code": "single",
                    "name": "Single"
                },
                {
                    "code": "family",
                    "name": "Family"
                }
            ],

            "options": [
                {
                    "code": "coverage",
                    "name": "Eyemed Basic Plus",
                    "carrier_code": "eyemed"
                },
                {
                    "code": "waive_coverage",
                    "name": "Waive Coverage",
                    "carrier_code": "_none"
                }
            ],

            "prices": [
                {
                    "plan_period_no": 11620,
                    "option_code": "coverage",
                    "tier_code": "single",
                    "amount": 12.02
                },
                {
                    "plan_period_no": 11620,
                    "option_code": "coverage",
                    "tier_code": "family",
                    "amount": 24.04
                }
            ]
        },
        {
            "plan_period_no": 11620,
            "code": "den",
            "name": "Dental",

            "tiers": [
                {
                    "code": "single",
                    "name": "Single"
                },
                {
                    "code": "family",
                    "name": "Family"
                }
            ],

            "options": [
                {
                    "code": "coverage",
                    "name": "Delta Premium Dental",
                    "carrier_code": "delta_dental_mi"
                },
                {
                    "code": "waive_coverage",
                    "name": "Waive Coverage",
                    "carrier_code": "_none"
                }
            ],

            "prices": [
                {
                    "plan_period_no": 11620,
                    "option_code": "coverage",
                    "tier_code": "single",
                    "amount": 13.03
                },
                {
                    "plan_period_no": 11620,
                    "option_code": "coverage",
                    "tier_code": "family",
                    "amount": 26.06
                }
            ]
        }
    ]
}