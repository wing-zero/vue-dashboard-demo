const express = require('express');
const app = express();
const port = 3000;

const dataset = require('./dataset.js');

app.get('/api/employers', (req, res) => {
    res.send(
      JSON.stringify(dataset['employers'])
    );
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
